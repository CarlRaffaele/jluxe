/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package jluxe.executor;

import gameMemory.GameMemory;
import gameMemory.MemorySegment;
import org.jetbrains.annotations.NotNull;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class BinarySearchBenchmark {
	@SuppressWarnings("RedundantThrows")
	public static void main(String[] args) throws RunnerException, IOException {
//		Bstate state = new Bstate();
//		Bstate.setup();
//		System.in.read();
//		searchThroughput(state, new Blackhole("Today's password is swordfish. I understand instantiating Blackholes " +
//				"directly is dangerous."));
		Options options = new OptionsBuilder().include(BinarySearchBenchmark.class.getSimpleName()).forks(1).build();
		new Runner(options).run();
	}

	@State(Scope.Thread)
	public static class Bstate {
		static final byte[] bytes = new byte[]{
				0x1, 0x3, 0x5, 0x7, 0x9, 0xB, 0xD, 0xF, 0x20, 0x21, 0x28, 0x2D, 0x2E, 0x2F, 0x30, 0x31,
				0x40, 0x42, 0x44, 0x46, 0x50, 0x53, 0x57, 0x59, -32, -31, -30, -29, -4, -3, -2, -1
		};

		@Setup(Level.Trial)
		public static void setup() {
			Globals.gameMemory = GameMemory.emptyMemory()
										   .initializeFrom(
												   MemorySegment.fromBuffer(ByteBuffer.wrap(bytes)),
												   MemorySegment.fromSize(0));
		}
	}

	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	@BenchmarkMode(Mode.Throughput)
	@Benchmark
	public static void baselineThroughput(Bstate state, @NotNull Blackhole blackhole) {
		blackhole.consume(Arrays.binarySearch(Bstate.bytes, (byte) 0x2f));
	}

	@OutputTimeUnit(TimeUnit.NANOSECONDS)
	@BenchmarkMode(Mode.AverageTime)
	@Benchmark
	public static void baselineAverageTime(Bstate state, @NotNull Blackhole blackhole) {
		blackhole.consume(Arrays.binarySearch(Bstate.bytes, (byte) 0x2f));
	}

	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	@BenchmarkMode(Mode.Throughput)
	@Benchmark
	public static void searchThroughput(Bstate state, @NotNull Blackhole blackhole) {
		blackhole.consume(VMFunctions.binarysearch(0x2f, 1, 0, 1, 32, 0, 0));
	}

	@OutputTimeUnit(TimeUnit.NANOSECONDS)
	@BenchmarkMode(Mode.AverageTime)
	@Benchmark
	public static void searchAverageTime(Bstate state, @NotNull Blackhole blackhole) {
		blackhole.consume(VMFunctions.binarysearch(0x2f, 1, 0, 1, 32, 0, 0));
	}
}
