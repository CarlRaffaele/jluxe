/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jluxe.executor;


import java.io.IOException;

public class GetIOSysBenchmark {
	public static void main(String[] args) throws IOException {
		org.openjdk.jmh.Main.main(args);
	}

//	@org.openjdk.jmh.annotations.State(Scope.Thread)
//	public static class State {
//		public final ByteBuffer bb = ByteBuffer.allocate(12);
//		public final IntConsumer c1 = i -> bb.putInt(0, i);
//		public final IntConsumer c2 = i -> bb.putInt(8, i);
//
//		@Setup(Level.Trial)
//		public static void setup() {
//			VMFunctions.setiosys(0, 222);
//		}
//	}

//	@OutputTimeUnit(TimeUnit.NANOSECONDS)
//	@BenchmarkMode(Mode.AverageTime)
//	@Benchmark
//	public static void measureGetIOsys1(State state) {
//		int[] ret = VMFunctions.getiosys2();
//		state.c1.accept(ret[0]);
//		state.c2.accept(ret[1]);
//	}

//	@OutputTimeUnit(TimeUnit.NANOSECONDS)
//	@BenchmarkMode(Mode.AverageTime)
//	@Benchmark
//	public static void measureGetIOSys2(State state) {
//		VMFunctions.getiosys(state.c1, state.c2);
//	}
}
