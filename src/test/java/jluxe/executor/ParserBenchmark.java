/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package jluxe.executor;


import blorbReader.GlulxHeader;
import blorbReader.UlxReader;
import gameMemory.GameMemory;
import gameMemory.MemorySegment;
import jluxe.parser.Parser;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

public class ParserBenchmark {
	@SuppressWarnings("RedundantThrows")
	public static void main(String[] args) throws RunnerException, URISyntaxException {
//		State state = new State();
//		state.setup();
//		state.setPosition();
//		parseWholeGameThroughput(state, new Blackhole("Today's password is swordfish. I understand instantiating Blackholes " +
//				"directly is dangerous."));
		Options options = new OptionsBuilder().include(ParserBenchmark.class.getSimpleName()).forks(1).build();
		new Runner(options).run();
	}

	@org.openjdk.jmh.annotations.State(Scope.Thread)
	public static class State {
		public GameMemory gameData;
		public GlulxHeader header;
		public Parser parser;

		@Setup(Level.Trial)
		public void setup() throws URISyntaxException {
			Path inFile = Path.of(this.getClass().getResource("/glulxercise.ulx").toURI());
			UlxReader game;
			try (FileChannel fileChannel = FileChannel.open(inFile)) {
				ByteBuffer bb = ByteBuffer.allocateDirect(117655);//Magic number found by running until it crashes for this specific game
				fileChannel.read(bb);
				bb.rewind();
				game = new UlxReader(MemorySegment.fromBuffer(bb));
				header = game.getGluxHeader();
				Globals.gluxHeader = header;
				gameData = GameMemory.fromSizes(header.END_MEM(), header.STACK_SIZE());
				gameData.initializeFrom(game.getGameData(), header.END_MEM());
				parser = new Parser((GameMemory) gameData.position(header.START_FUNCTION_ADDRESS()));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		@Setup(Level.Invocation)
		public void setPosition() {
			gameData.position(header.START_FUNCTION_ADDRESS());
		}
	}

//	@OutputTimeUnit(TimeUnit.SECONDS)
//	@BenchmarkMode(Mode.Throughput)
//	@Benchmark
//	public static void baselineThroughput(@NotNull State state, Blackhole blackhole) {
//		while (state.gameData.hasRemaining()) {
//			blackhole.consume(state.gameData.get());
//		}
//	}
//
//	@OutputTimeUnit(TimeUnit.MILLISECONDS)
//	@BenchmarkMode(Mode.AverageTime)
//	@Benchmark
//	public static void baselineAverageTime(@NotNull State state, Blackhole blackhole) {
//		while (state.gameData.hasRemaining()) {
//			blackhole.consume(state.gameData.get());
//		}
//	}

	@OutputTimeUnit(TimeUnit.SECONDS)
	@BenchmarkMode(Mode.Throughput)
	@Benchmark
	public static void parseWholeGameThroughput(State state, Blackhole blackhole) {
		while (state.gameData.hasRemaining()) {
			byte test = state.gameData.getByte(state.gameData.position());
			if (test == -64 || test == -63) {
				blackhole.consume(state.parser.parseFunction(state.gameData));
			} else {
//				blackhole.consume(state.parser.parseOP(state.gameData));
			}
		}
	}

	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	@BenchmarkMode(Mode.AverageTime)
	@Benchmark
	public static void parseWholeGameAverageTime(State state, Blackhole blackhole) {
		while (state.gameData.hasRemaining()) {
			byte test = state.gameData.getByte(state.gameData.position());
			if (test == -64 || test == -63) {
				blackhole.consume(state.parser.parseFunction(state.gameData));
			} else {
//				blackhole.consume(state.parser.parseOP(state.gameData));
			}
		}
	}
}
