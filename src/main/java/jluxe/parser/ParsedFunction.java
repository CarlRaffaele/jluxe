/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package jluxe.parser;

import gameMemory.Function;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.BiFunction;

/**
 * The Parsed Function class is the resulting object after the jluxe.parser parses a function.
 * <p>
 * This is similar to the ParsedInstruction class, but for functions instead.
 */
public record ParsedFunction(@NotNull Functions type,
							 @Nullable BiFunction<Integer, Integer, Integer> acceleratedFunction,
							 int localsCount,
							 int localsFormatAddress,
							 int localsFormatLength) implements Function {

	@Override
	public boolean isAccelerated() {
		return acceleratedFunction != null;
	}

	@Override
	public int runAcceleratedFunction(int arg1, int arg2) {
		if (acceleratedFunction == null) {
			throw new IllegalStateException("Tried to call a non-accelerated function");
		}
		return acceleratedFunction.apply(arg1, arg2);
	}

	@Override
	public boolean isStackFunction() {
		return type == Functions.STACK;
	}
}
