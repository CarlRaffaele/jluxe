/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package jluxe.parser;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import gameMemory.CallStubType;
import gameMemory.MemorySegment;
import jluxe.executor.Globals;
import jluxe.executor.LoadStoreModeImpl;
import jluxe.executor.VMFunctions;
import org.jetbrains.annotations.NotNull;

import java.util.*;

class DecodingTable {
	private final int length;
	private final int numNodes;
	private final int root;
	private final int startPtr;
	private final Map<String, Node> table;

	DecodingTable(MemorySegment first12Bytes, int startPtr) {
		Objects.requireNonNull(first12Bytes);
		if (startPtr <= 0) {
			throw new IllegalArgumentException("Starting address of Decoding Table is an invalid number: " + startPtr);
		}
		this.length = first12Bytes.getInt();
		this.numNodes = first12Bytes.getInt();
		this.root = first12Bytes.getInt();

		startPtr += 12; // 12 bytes for these first three ints that are read
		this.startPtr = startPtr;
		this.table = new HashMap<>();
	}


	int getAddress() {
		return startPtr;
	}

	void decodeAndStreamString(MemorySegment gameData) {
		String test = "";
		Node found;
		while (true) {
			byte testbyte = gameData.getByte();
			for (int i = 0; i < 8; i++) {
				test = (testbyte & 1) + test;
				testbyte >>= 1;
				if (this.table.containsKey(test)) {
					found = this.table.get(test);
					if (found == null) { //Exit Node
						return;
					} else if (found.isIndirect()) { //Function or String pointer node
						pushType10(gameData.position(), i);
						if (Functions.isFunction(gameData, found.getAddress())) {
							VMFunctions.call(found.getAddress(), found.getArgs(), LoadStoreModeImpl.ZERO, 0);
						} else {
							gameData.position(found.getAddress());
							Strings.fromCode(gameData.getByte())
								   .parseAndStream(gameData, Globals.IOSystemStream, this);
						}
						Globals.gameMemory.getCurrentCallFrame().popCallStubAndStoreValue(0); // Pop off the type-10
					} else if (found.getCodePoints().size() > 0) { //String or Character Node
						for (Integer integer : found.getCodePoints()) {
							Globals.IOSystemStream.accept(integer);
						}
					} else {
						throw new IllegalArgumentException("Unknown node type: " + found);
					}
					test = "";
				}

			}
		}
	}

	private static void pushType10(int byteAddr, int bitNum) {
		Globals.gameMemory
				.getCurrentCallFrame()
				.pushCallStub(CallStubType.RESUME_PRINTING_COMPRESSED_STRING,
						bitNum,
						byteAddr,
						Globals.gameMemory.getCurrentCallFrame().getFramePtr());
	}

	void fillTable(MemorySegment data) {
		this.traverseTree(data, "", this.startPtr);
	}

	@SuppressFBWarnings(justification = "Intentional", value = {"SF_SWITCH_FALLTHROUGH"})
	private void traverseTree(MemorySegment treeData, String branch, int startAddress) {
		Objects.requireNonNull(treeData);
		branch = Objects.requireNonNullElse(branch, "");

		treeData.position(startAddress);
		boolean isDouble = true;
		List<Integer> codePoints;
		switch (treeData.getByte()) {
			case 0x0: // Branch node
				int left = treeData.getInt();
				int right = treeData.getInt();
				this.traverseTree(treeData, "0" + branch, left); // Left
				this.traverseTree(treeData, "1" + branch, right); // Right
				return;
			case 0x1: // String terminator
				this.table.put(branch, null);
				return;
			case 0x2: // Single character
				this.table.put(branch, new Node(treeData.getByte() & 0xff));
				return;
			case 0x3: // C-style string
				codePoints = new ArrayList<>();
				while (true) {
					byte toAdd = treeData.getByte();
					if (toAdd == 0x00) {
						break;
					}
					codePoints.add((int) toAdd);
				}
				this.table.put(branch, new Node(codePoints));
				return;
			case 0x4: // Unicode character
				this.table.put(branch, new Node(treeData.getInt()));
				return;
			case 0x5: // C-style Unicode string
				codePoints = new ArrayList<>();
				while (true) {
					int toAdd = treeData.getInt();
					if (toAdd == 0x00000000) {
						break;
					}
					codePoints.add(toAdd);
				}
				this.table.put(branch, new Node(codePoints));
				return;
			case 0x8: // Indirect or
				isDouble = false;
				// fall through
			case 0x9: // double indirect reference
				this.table.put(branch, new Node(treeData.getInt(), isDouble));
				return;
			case 0xA: // Indirect with args or
				isDouble = false;
				// fall through
			case 0xB: // Double indirect with args
				int addr = treeData.getInt();
				int numargs = treeData.getInt();
				int[] args = new int[numargs];
				for (int i = 0; i < numargs; i++) {
					args[i] = treeData.getInt();
				}
				this.table.put(branch, new Node(addr, isDouble, args));
				return;
			default:
				System.err.println(this);
				throw new IllegalArgumentException("Unknown Node type");
		}
	}

	@Override
	public String toString() {
		return "DecodingTable{" +
				"length=" + length +
				", numNodes=" + numNodes +
				", root=" + root +
				", startPtr=" + startPtr +
				", table=" + table +
				'}';
	}

	private static class Node {
		private final boolean isDouble;
		private final boolean isFunction;
		@NotNull
		private final List<Integer> codePoints;
		private int[] args;
		private final int address;

		/**
		 * Single character constructor
		 *
		 * @param codePoint The Unicode code point of the character
		 */
		Node(int codePoint) {
			this(Collections.singletonList(codePoint));
		}

		/**
		 * String constructor
		 *
		 * @param codePoints The codepoints that makes up the string for this node
		 */
		Node(@NotNull List<Integer> codePoints) {
			this.codePoints = codePoints;
			this.isFunction = false;
			this.isDouble = false;
			address = -1;
		}

		/**
		 * Function with args constructor
		 *
		 * @param addr     The address of the function
		 * @param isDouble True if this function address is a memory address that contains the address of the
		 *                 function to call
		 * @param args     The arguments to pass into the function
		 */
		Node(int addr, boolean isDouble, int[] args) {
			this(addr, isDouble);
			this.args = args;
		}

		/**
		 * Function without args constructor
		 *
		 * @param addr     The address of the function
		 * @param isDouble True if this function address is a memory address that contains the address of the
		 *                 function to call
		 */
		Node(int addr, boolean isDouble) {
			this.address = addr;
			this.isDouble = isDouble;
			this.isFunction = true;
			this.codePoints = Collections.emptyList();
		}

		int getAddress() {
			return this.isDouble ? Globals.gameMemory.getInt(this.address) : this.address;
		}

		int[] getArgs() {
			return args == null ? new int[0] : args;
		}

		@NotNull
		List<Integer> getCodePoints() {
			return codePoints;
		}

		boolean isIndirect() {
			return this.isFunction;
		}

	}
}
