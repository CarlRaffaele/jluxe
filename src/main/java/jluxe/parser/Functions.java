/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package jluxe.parser;

import gameMemory.MemorySegment;
import org.jetbrains.annotations.NotNull;

enum Functions {
	STACK(0xC0),
	LOCALS(0xC1);

	final int CODE;

	Functions(int code) {
		CODE = code;
	}

	private static Functions fromMemory(MemorySegment gameData, int position) {
		return fromCode(gameData.getByte(position) & 0xff);
	}

	private static Functions fromCode(int code) {
		return switch (code) {
			case 0xC0 -> STACK;
			case 0xC1 -> LOCALS;
			default -> null;
		};
	}

	static boolean isFunction(MemorySegment gameData, int address) {
		return fromMemory(gameData, address) != null;
	}

	/**
	 * Parses a bytebuffer that's located at the start of a function object. It returns a Parsed Function object that
	 * contains all the function information.
	 *
	 * @param gameData The game file where the first read byte will be the function byte
	 * @return A ParsedFunction object containing all the information about the function
	 */
	@NotNull
	static ParsedFunction parseFunction(@NotNull MemorySegment gameData) {
		Functions type = fromCode(gameData.getByte() & 0xff);
		if (type == null) {
			throw new IllegalArgumentException(
					"Tried to function-parse a non-function. Type:  at address: " + (gameData.position() - 1));
		}
		int start = gameData.position();
		int ret = 0;
		while (true) {
			short temp = gameData.getShort();
			if ((temp & 0xfb00) != 0) { //size != 4
				throw new IllegalArgumentException("Function trying to use non-4 byte locals");
			}
			ret += temp & 0xff;
			if (temp == 0) {
				return new ParsedFunction(type,
						null,
						ret,
						start,
						gameData.position() - start);
			}
		}
	}
}
