/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package jluxe.parser;

import gameMemory.MemorySegment;
import org.jetbrains.annotations.Nullable;

import java.util.function.IntConsumer;

enum Strings implements StringStreamer {
	C_STYLE(0xE0) {
		@Override
		public void parseAndStream(MemorySegment gameData, IntConsumer stream, @Nullable DecodingTable dt) {
			while (true) {
				byte next = gameData.getByte();
				if (next == 0) {
					return;
				}
				stream.accept(next & 0xff);
			}
		}
	},
	COMPRESSED(0xE1) {
		@Override
		public void parseAndStream(MemorySegment gameData, IntConsumer stream, @Nullable DecodingTable dt) {
			if (dt == null) {
				throw new IllegalStateException("Tried to parse a compressed string without a decoding table");
			}
			dt.decodeAndStreamString(gameData);
		}
	},
	UNICODE(0xE2) {
		@Override
		public void parseAndStream(MemorySegment gameData, IntConsumer stream, @Nullable DecodingTable dt) {
			//Three bytes of padding
			gameData.getByte();
			gameData.getByte();
			gameData.getByte();
			while (true) {
				int toAdd = gameData.getInt();
				if (toAdd == 0x00000000) {
					return;
				}
				stream.accept(toAdd);
			}
		}
	};

	final int CODE;

	Strings(int code) {
		this.CODE = code;
	}

	public static Strings fromCode(byte code) {
		return switch (code) {
			case (byte) 0xE0 -> C_STYLE;
			case (byte) 0xE1 -> COMPRESSED;
			case (byte) 0xE2 -> UNICODE;
			default -> throw new IllegalArgumentException("Unknown string code: " + code);
		};
	}
}
