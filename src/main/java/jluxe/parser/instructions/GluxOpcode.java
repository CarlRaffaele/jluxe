/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package jluxe.parser.instructions;

/**
 * A class defining a GLK opcode. VMFunctions uses this to define all the different opcodes and the information
 * the parser needs to parse the code properly.
 */
public abstract class GluxOpcode implements Opcode {
	protected final String name;
	protected final boolean pureFunction;

	GluxOpcode(String name, boolean pureFunction) {
		this.name = name;
		this.pureFunction = pureFunction;
	}

	@Override
	public String toString() {
		return "GluxOpcode{" +
				"name='" + name + '\'' +
				'}';
	}
}
