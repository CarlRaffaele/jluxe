/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package jluxe.parser;

import gameMemory.CallStubType;
import gameMemory.Function;
import gameMemory.GameMemory;
import gameMemory.MemorySegment;
import gnu.trove.map.hash.TIntObjectHashMap;
import jluxe.executor.Globals;
import jluxe.parser.instructions.Instruction;
import jluxe.parser.instructions.Opcode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.ByteBuffer;
import java.util.function.BiFunction;
import java.util.function.IntConsumer;

/**
 * This static class provides functions to parse different types of data out of the instruction segment of the game.
 */
public class Parser {
	private static final int INITIAL_OP_CACHE_SIZE = 2048;
	private static final Logger LOG = LogManager.getLogger(Parser.class);
	private static final Opcode[] OPS;
	private final TIntObjectHashMap<Instruction> opCache;
	private final TIntObjectHashMap<Function> functionCache;
	private final GameMemory gameData;
	private @Nullable DecodingTable dt;

	static {
		//Done through reflection because the array is generated through compile-time annotations
		Opcode[] OPS1;
		try {
			OPS1 = (Opcode[]) Class.forName("jluxe.parser.instructions.GeneratedOpcodes").getField("OPS").get(null);
		} catch (NoSuchFieldException | ClassNotFoundException | IllegalAccessException e) {
			throw new IllegalStateException("Unable to acquire OPS dictionary", e);
		}
		OPS = OPS1;
	}

	public Parser(GameMemory gameData, boolean fillCache) {
		opCache = new TIntObjectHashMap<>(INITIAL_OP_CACHE_SIZE, 1);
		functionCache = new TIntObjectHashMap<>(INITIAL_OP_CACHE_SIZE, 1);
		this.gameData = gameData;
		if (fillCache) {
			while (true) {
				int startAddress = gameData.position();
				if (Functions.isFunction(gameData, startAddress)) {
					functionCache.put(startAddress, parseFunction(gameData));
				} else {
					try {
						opCache.put(startAddress, OPS[getOPCode(gameData)].parse(gameData));
					} catch (Exception e) {
						LOG.debug("Finished building parser at: " + gameData.position());
						break;
					}
				}
			}
		}
	}

	public Parser(GameMemory gameData) {
		this(gameData, true);
	}

	//The following decoding table instructions are shortcuts to avoid having the Execution Functions class having
	//to know about the decoding table
	public int getDecodingTableAddress() {
		if (dt == null) {
			return 0;
		}
		return dt.getAddress();
	}

	public void setDecodingTable(int address) {
		LOG.debug("Setting decoding table to: " + address);
		int save = gameData.position();
		dt = new DecodingTable(gameData, address);
		dt.fillTable(gameData);
		gameData.position(save);
	}

	/**
	 * Helper function to stream a string from a given address. The ByteBuffer is assumed to be the memory stored by
	 * the GLKGlobals class.
	 *
	 * @param address The address of the start of the string
	 */
	public void parseAndStreamString(int address) {
		gameData.getCurrentCallFrame().pushCallStub(CallStubType.RESUME_EXECUTION_AFTER_STRING, 0);
		gameData.position(address);
		Strings.fromCode(gameData.getByte())
			   .parseAndStream(gameData, Globals.IOSystemStream, dt);
		gameData.getCurrentCallFrame().popCallStubAndStoreValue(0); // pop off the type-11 call stub
	}

	/**
	 * Parse an instruction given the gameData's position. If this is the first time the instruction is seen then it
	 * will parse it completely, otherwise it will pull the instruction out of the cache.
	 *
	 * @param gameData The instruction MemoryBuffer with the current position at the start of the instruction to parse
	 * @return The ParsedInstruction or the CachedInstruction
	 */
	public @NotNull Instruction parseOP(@NotNull MemorySegment gameData) {
		int opStart = gameData.position();
		Instruction instruction = opCache.get(opStart);
		gameData.position(instruction.nextInstructionAddress());
		return instruction;
	}

	/**
	 * Returns the opcode of the instruction at the current position of the gameData.
	 *
	 * @param gameData A {@link ByteBuffer} whose current position is the start of the desired opcode
	 * @return An integer in the valid opcode range
	 * @see jluxe.parser.instructions.GluxOpcode
	 */
	private static int getOPCode(@NotNull MemorySegment gameData) {
		return switch (gameData.getByte(gameData.position()) & 0xC0) {
			case 0b10000000 -> gameData.getShort() & 0x7fff; //opcode size of 2
			case 0b11000000 -> gameData.getInt() & 0x3fffffff; //opcode size of 4
			default -> gameData.getByte(); //opcode size of 1
		};
	}

	/**
	 * Parses a string character by character from the given address. Care should be taken when using this function as
	 * it is not intended functionality of the spec to be able to pass an entire string in the VM. Code can execute
	 * between each character read and other strings can be printed.
	 *
	 * @param address The address of the start of the string. The first byte read should be the type of string
	 * @return A Java String object created from reading the gluxe string data
	 */
	@NotNull
	public String parseString(int address) {
		StringBuilder s = new StringBuilder();
		IntConsumer save = Globals.IOSystemStream;
		Globals.IOSystemStream = s::appendCodePoint;
		Strings.fromCode(gameData.getByte())
			   .parseAndStream(gameData.position(address), Globals.IOSystemStream, dt);
		Globals.IOSystemStream = save;
		return s.toString();
	}

	public void enableAcceleratedFunction(int address, BiFunction<Integer, Integer, Integer> replacement) {
		ParsedFunction old = (ParsedFunction) functionCache.get(address);
		if (old != null) {
			functionCache.put(address, new ParsedFunction(old.type(),
					replacement,
					old.localsCount(),
					old.localsFormatAddress(),
					old.localsFormatLength()));
		} else {
			functionCache.put(address, new ParsedFunction(Functions.STACK, replacement, 0, 0, 0));
		}
	}

	public @NotNull Function parseFunction(int address) {
		Function ret = functionCache.get(address);
		gameData.position(ret.localsFormatAddress() + ret.localsFormatLength());
		return ret;
	}

	/**
	 * Parses a bytebuffer that's located at the start of a function object. It returns a Parsed Function object that
	 * contains all the function information.
	 *
	 * @param gameData The game file where the first read byte will be the function byte
	 * @return A ParsedFunction object containing all the information about the function
	 */
	@NotNull
	@Contract("_ -> new")
	public ParsedFunction parseFunction(@NotNull MemorySegment gameData) {
		return Functions.parseFunction(gameData);
	}
}
