/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package jluxe.executor;

import blorbReader.QuetzalChunkType;
import blorbReader.QuetzalFileReader;
import gameMemory.MemorySegment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public record RestorePackage(@Nullable MemorySegment uMem,
							 @Nullable MemorySegment cMem,
							 @NotNull MemorySegment stks) {
	public RestorePackage(QuetzalFileReader fr) {
		this(fr.getChunks().get(QuetzalChunkType.UNCOMPRESSED_MEMORY),
				fr.getChunks().get(QuetzalChunkType.COMPRESSED_MEMORY),
				fr.getChunks().get(QuetzalChunkType.STACKS));
	}
}
