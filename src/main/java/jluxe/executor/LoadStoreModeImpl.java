/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package jluxe.executor;

import gameMemory.CallStubType;
import gameMemory.MemorySegment;

/**
 * An enum to action the different load and store modes of arguments in the virtual machine. UNUSED_1 and UNUSED_2
 * are included to match the numbering with their IDs.
 */
public enum LoadStoreModeImpl implements LoadStoreMode {
	ZERO(true) {
		@Override
		public CallStubType toCallStubType() {
			return CallStubType.DISCARD;
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return 0;
		}

		@Override
		public int loadAsInt(int argument) {
			return 0;
		}

		@Override
		public short loadAsShort(int argument) {
			return 0;
		}

		@Override
		public byte loadAsByte(int argument) {
			return 0;
		}

		@Override
		public int getStoreArgument(MemorySegment gameData) {
			return 0;
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {

		}

		@Override
		public void storeAsShort(int storeAddress, int value) {

		}

		@Override
		public void storeAsByte(int storeAddress, int value) {

		}
	},
	CONSTANT_BYTE(true) {
		@Override
		public CallStubType toCallStubType() {
			throw new IllegalArgumentException();
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return gameData.getByte();
		}

		@Override
		public int loadAsInt(int argument) {
			return argument;
		}

		@Override
		public short loadAsShort(int argument) {
			return (short) argument;
		}

		@Override
		public byte loadAsByte(int argument) {
			return (byte) argument;
		}

		@Override
		public int getStoreArgument(MemorySegment gameData) {
			throw new IllegalStoreMode();
		}
	},
	CONSTANT_SHORT(true) {
		@Override
		public CallStubType toCallStubType() {
			throw new IllegalArgumentException();
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return gameData.getShort();
		}

		@Override
		public int loadAsInt(int argument) {
			return argument;
		}

		@Override
		public short loadAsShort(int argument) {
			return (short) argument;
		}

		@Override
		public byte loadAsByte(int argument) {
			return (byte) argument;
		}

		@Override
		public int getStoreArgument(MemorySegment gameData) {
			throw new IllegalStoreMode();
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			throw new IllegalStoreMode();
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			throw new IllegalStoreMode();
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			throw new IllegalStoreMode();
		}
	},
	CONSTANT_INT(true) {
		@Override
		public CallStubType toCallStubType() {
			throw new IllegalArgumentException();
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return gameData.getInt();
		}

		@Override
		public int loadAsInt(int argument) {
			return argument;
		}

		@Override
		public short loadAsShort(int argument) {
			return (short) argument;
		}

		@Override
		public byte loadAsByte(int argument) {
			return (byte) argument;
		}

		@Override
		public int getStoreArgument(MemorySegment gameData) {
			throw new IllegalStoreMode();
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			throw new IllegalStoreMode();
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			throw new IllegalStoreMode();
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			throw new IllegalStoreMode();
		}
	},
	UNUSED_1(false) {
		@Override
		public CallStubType toCallStubType() {
			throw new IllegalArgumentException();
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			throw new IllegalArgumentException();
		}

		@Override
		public int loadAsInt(int argument) {
			throw new IllegalArgumentException();
		}

		@Override
		public short loadAsShort(int argument) {
			throw new IllegalArgumentException();
		}

		@Override
		public byte loadAsByte(int argument) {
			throw new IllegalArgumentException();
		}

		@Override
		public int getStoreArgument(MemorySegment gameData) {
			throw new IllegalStoreMode();
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			throw new IllegalStoreMode();
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			throw new IllegalStoreMode();
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			throw new IllegalStoreMode();
		}
	},
	MEMORY_BYTE_ADDRESS(false) {
		@Override
		public CallStubType toCallStubType() {
			return CallStubType.MAIN_MEMORY;
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return gameData.getByte() & 0xff;
		}

		@Override
		public int loadAsInt(int argument) {
			return Globals.gameMemory.getInt(argument);
		}

		@Override
		public short loadAsShort(int argument) {
			return Globals.gameMemory.getShort(argument);
		}

		@Override
		public byte loadAsByte(int argument) {
			return Globals.gameMemory.getByte(argument);
		}

		@Override
		public int getStoreArgument(MemorySegment gameData) {
			return gameData.getByte() & 0xff;
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, value);
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, (char) value);
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, (byte) value);
		}
	},
	MEMORY_SHORT_ADDRESS(false) {
		@Override
		public CallStubType toCallStubType() {
			return CallStubType.MAIN_MEMORY;
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return gameData.getShort() & 0xffff;
		}

		@Override
		public int loadAsInt(int argument) {
			return Globals.gameMemory.getInt(argument);
		}

		@Override
		public short loadAsShort(int argument) {
			return Globals.gameMemory.getShort(argument);
		}

		@Override
		public byte loadAsByte(int argument) {
			return Globals.gameMemory.getByte(argument);
		}

		@Override
		public int getStoreArgument(MemorySegment gameData) {
			return gameData.getByte() & 0xff;
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, value);
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, (char) value);
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, (byte) value);
		}
	},
	MEMORY_INT_ADDRESS(false) {
		@Override
		public CallStubType toCallStubType() {
			return CallStubType.MAIN_MEMORY;
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return gameData.getInt();
		}

		@Override
		public int loadAsInt(int argument) {
			return Globals.gameMemory.getInt(argument);
		}

		@Override
		public short loadAsShort(int argument) {
			return Globals.gameMemory.getShort(argument);
		}

		@Override
		public byte loadAsByte(int argument) {
			return Globals.gameMemory.getByte(argument);
		}

		@Override
		public int getStoreArgument(MemorySegment gameData) {
			return gameData.getByte() & 0xff;
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, value);
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, (char) value);
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, (byte) value);
		}
	},
	STACK_POP(false) {
		@Override
		public CallStubType toCallStubType() {
			return CallStubType.PUSH_ON_STACK;
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return 0;
		}

		@Override
		public int loadAsInt(int argument) {
			return Globals.gameMemory.getCurrentCallFrame().pop();
		}

		@Override
		public short loadAsShort(int argument) {
			return (short) Globals.gameMemory.getCurrentCallFrame().pop();
		}

		@Override
		public byte loadAsByte(int argument) {
			return (byte) Globals.gameMemory.getCurrentCallFrame().pop();
		}

		@Override
		public int getStoreArgument(MemorySegment gameData) {
			return 0;
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			Globals.gameMemory.getCurrentCallFrame().push(value);
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			Globals.gameMemory.getCurrentCallFrame().push(value & 0xffff);
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			Globals.gameMemory.getCurrentCallFrame().push(value & 0xff);
		}
	},
	CALLFRAME_BYTE_ADDRESS(false) {
		@Override
		public CallStubType toCallStubType() {
			return CallStubType.LOCAL;
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return gameData.getByte() & 0xff;
		}

		@Override
		public int loadAsInt(int argument) {
			return Globals.gameMemory.getCurrentCallFrame().getLocal(argument);
		}

		@Override
		public short loadAsShort(int argument) {
			return (short) Globals.gameMemory.getCurrentCallFrame().getLocal(argument);
		}

		@Override
		public byte loadAsByte(int argument) {
			return (byte) Globals.gameMemory.getCurrentCallFrame().getLocal(argument);
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			Globals.gameMemory.getCurrentCallFrame().setLocal(storeAddress, value);
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			throw new IllegalArgumentException("Tried to store a short in the call frame");
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			throw new IllegalArgumentException("Tried to store a byte in the call frame");
		}
	},
	CALLFRAME_SHORT_ADDRESS(false) {
		@Override
		public CallStubType toCallStubType() {
			return CallStubType.LOCAL;
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return gameData.getShort() & 0xffff;
		}

		@Override
		public int loadAsInt(int argument) {
			return Globals.gameMemory.getCurrentCallFrame().getLocal(argument);
		}

		@Override
		public short loadAsShort(int argument) {
			return (short) Globals.gameMemory.getCurrentCallFrame().getLocal(argument);
		}

		@Override
		public byte loadAsByte(int argument) {
			return (byte) Globals.gameMemory.getCurrentCallFrame().getLocal(argument);
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			Globals.gameMemory.getCurrentCallFrame().setLocal(storeAddress, value);
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			throw new IllegalArgumentException("Tried to store a short in the call frame");
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			throw new IllegalArgumentException("Tried to store a byte in the call frame");
		}
	},
	CALLFRAME_INT_ADDRESS(false) {
		@Override
		public CallStubType toCallStubType() {
			return CallStubType.LOCAL;
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return gameData.getInt();
		}

		@Override
		public int loadAsInt(int argument) {
			return Globals.gameMemory.getCurrentCallFrame().getLocal(argument);
		}

		@Override
		public short loadAsShort(int argument) {
			return (short) Globals.gameMemory.getCurrentCallFrame().getLocal(argument);
		}

		@Override
		public byte loadAsByte(int argument) {
			return (byte) Globals.gameMemory.getCurrentCallFrame().getLocal(argument);
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			Globals.gameMemory.getCurrentCallFrame().setLocal(storeAddress, value);
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			throw new IllegalArgumentException("Tried to store a short in the call frame");
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			throw new IllegalArgumentException("Tried to store a byte in the call frame");
		}
	},
	UNUSED_2(false) {
		@Override
		public CallStubType toCallStubType() {
			throw new IllegalArgumentException();
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			throw new IllegalArgumentException();
		}

		@Override
		public int loadAsInt(int argument) {
			throw new IllegalArgumentException();
		}

		@Override
		public short loadAsShort(int argument) {
			throw new IllegalArgumentException();
		}

		@Override
		public byte loadAsByte(int argument) {
			throw new IllegalArgumentException();
		}

		@Override
		public int getStoreArgument(MemorySegment gameData) {
			throw new IllegalStoreMode();
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			throw new IllegalStoreMode();
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			throw new IllegalStoreMode();
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			throw new IllegalStoreMode();
		}
	},
	RAM_BYTE_ADDRESS(false) {
		@Override
		public CallStubType toCallStubType() {
			return CallStubType.MAIN_MEMORY;
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return (gameData.getByte() & 0xff) + Globals.gluxHeader.RAM_START();
		}

		@Override
		public int loadAsInt(int argument) {
			return Globals.gameMemory.getInt(argument);
		}

		@Override
		public short loadAsShort(int argument) {
			return Globals.gameMemory.getShort(argument);
		}

		@Override
		public byte loadAsByte(int argument) {
			return Globals.gameMemory.getByte(argument);
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, value);
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, (char) value);
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, (byte) value);
		}
	},
	RAM_SHORT_ADDRESS(false) {
		@Override
		public CallStubType toCallStubType() {
			return CallStubType.MAIN_MEMORY;
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return (gameData.getShort() & 0xffff) + Globals.gluxHeader.RAM_START();
		}

		@Override
		public int loadAsInt(int argument) {
			return Globals.gameMemory.getInt(argument);
		}

		@Override
		public short loadAsShort(int argument) {
			return Globals.gameMemory.getShort(argument);
		}

		@Override
		public byte loadAsByte(int argument) {
			return Globals.gameMemory.getByte(argument);
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, value);
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, (char) value);
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, (byte) value);
		}
	},
	RAM_INT_ADDRESS(false) {
		@Override
		public CallStubType toCallStubType() {
			return CallStubType.MAIN_MEMORY;
		}

		@Override
		public int getLoadArgument(MemorySegment gameData) {
			return gameData.getInt() + Globals.gluxHeader.RAM_START();
		}

		@Override
		public int loadAsInt(int argument) {
			return Globals.gameMemory.getInt(argument);
		}

		@Override
		public short loadAsShort(int argument) {
			return Globals.gameMemory.getShort(argument);
		}

		@Override
		public byte loadAsByte(int argument) {
			return Globals.gameMemory.getByte(argument);
		}

		@Override
		public void storeAsInt(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, value);
		}

		@Override
		public void storeAsShort(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, (char) value);
		}

		@Override
		public void storeAsByte(int storeAddress, int value) {
			Globals.gameMemory.put(storeAddress, (byte) value);
		}
	};

	private final boolean isConstant;

	LoadStoreModeImpl(boolean isConstant) {
		this.isConstant = isConstant;
	}

	@Override
	public boolean isConstant() {
		return isConstant;
	}

}
