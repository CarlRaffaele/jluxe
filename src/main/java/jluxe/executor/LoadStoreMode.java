/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package jluxe.executor;

import gameMemory.CallStubType;
import gameMemory.MemorySegment;

public interface LoadStoreMode {

	static LoadStoreMode fromID(int ID) {
		return LoadStoreModeImpl.values()[ID];
	}

	static LoadStoreMode fromLowerHalf(byte ID) {
		return LoadStoreModeImpl.values()[ID & 0xf];
	}

	static LoadStoreMode fromUpperHalf(byte ID) {
		return LoadStoreModeImpl.values()[(ID >> 4) & 0xf];
	}

	boolean isConstant();

	default CallStubType toCallStubType() {
		throw new IllegalArgumentException();
	}

	int getLoadArgument(MemorySegment gameData);

	int loadAsInt(int argument);

	short loadAsShort(int argument);

	byte loadAsByte(int argument);

	default int getStoreArgument(MemorySegment gameData) {
		return getLoadArgument(gameData);
	}

	default void storeAsInt(int storeAddress, int value) {
		throw new IllegalStoreMode();
	}

	default void storeAsShort(int storeAddress, int value) {
		throw new IllegalStoreMode();
	}

	default void storeAsByte(int storeAddress, int value) {
		throw new IllegalStoreMode();
	}

	class IllegalStoreMode extends IllegalArgumentException {

	}
}
