/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package jluxe.executor;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Objects;

class ByteArraySearchOptions implements SearchOptions<byte[]> {
	private final byte[] key;
	private final boolean zeroKeyTerminates;
	private final boolean returnIndex;

	ByteArraySearchOptions(int key, int keySize, int options) {
		zeroKeyTerminates = (options & 2) == 2;
		returnIndex = (options & 4) == 4;
		this.key = new byte[keySize];
		if ((options & 1) == 1) { // indirect key
			Globals.gameMemory.getBytes(key, this.key);
		} else { // direct key
			switch (keySize) {
				case 1 -> this.key[0] = (byte) key;
				case 2 -> {
					this.key[0] = (byte) (key >> 8);
					this.key[1] = (byte) key;
				}
				case 4 -> {
					this.key[0] = (byte) (key >> 24);
					this.key[1] = (byte) (key >> 16);
					this.key[2] = (byte) (key >> 8);
					this.key[3] = (byte) key;
				}
				default -> throw new IllegalArgumentException("Key size is invalid type: " + keySize);
			}
		}
	}

	@Override
	public boolean isZeroKey(@NotNull byte[] key) {
		for (byte b : key) {
			if (b != 0) {
				return false;
			}
		}
		return true;
	}

	@Override
	@Contract(pure = true)
	public int getFailValue() {
		return this.returnIndex ? -1 : 0;
	}

	@Override
	public byte[] getKey() {
		return key;
	}

	@Override
	public byte[] getKey(int address) {
		Objects.requireNonNull(this.key);
		byte[] key = new byte[this.key.length];
		Globals.gameMemory.getBytes(address, key);
		return key;
	}

	@Override
	@Contract(pure = true)
	public boolean keyMatches(byte[] key) {
		return Arrays.equals(this.key, key);
	}

	@Override
	public boolean returnIndex() {
		return returnIndex;
	}

	@Override
	public boolean zeroKeyTerminates() {
		return zeroKeyTerminates;
	}
}
