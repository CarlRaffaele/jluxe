/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package jluxe.executor;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Nullable;

import java.util.function.BiFunction;
import java.util.function.Function;

public class AcceleratedFunctions {
	private static final Logger LOG = LogManager.getLogger(AcceleratedFunctions.class);
	private static int PARAM_0_classes_table;
	private static int PARAM_1_indiv_prop_start;
	private static int PARAM_2_class_metaclass;
	private static int PARAM_3_object_metaclass;
	private static int PARAM_4_routine_metaclass;
	private static int PARAM_5_string_metaclass;
	private static int PARAM_6_self;
	private static int PARAM_7_num_attr_bytes;
	private static int PARAM_8_cpv__start;

	static void setParameter(int id, int value) {
		switch (id) {
			case 0 -> PARAM_0_classes_table = value;
			case 1 -> PARAM_1_indiv_prop_start = value;
			case 2 -> PARAM_2_class_metaclass = value;
			case 3 -> PARAM_3_object_metaclass = value;
			case 4 -> PARAM_4_routine_metaclass = value;
			case 5 -> PARAM_5_string_metaclass = value;
			case 6 -> PARAM_6_self = value;
			case 7 -> PARAM_7_num_attr_bytes = value;
			case 8 -> PARAM_8_cpv__start = value;
			default -> LOG.info("Tried to set unknown acceleration parameter: " + id + " to value:" +
					" " + value);
		}
	}

	private static boolean obj_in_class(int obj) {
		return Globals.gameMemory.getInt(obj + 13 + PARAM_7_num_attr_bytes) == PARAM_2_class_metaclass;
	}

	static @Nullable BiFunction<Integer, Integer, Integer> getParsedFunction(int id) {
		return switch (id) {
			case 1 -> AcceleratedFunctions::func_1_Z__Region;
			case 2 -> AcceleratedFunctions::func_2_CP__Tab;
			case 3 -> AcceleratedFunctions::func_3_RA__Pr;
			case 4 -> AcceleratedFunctions::func_4_RL__Pr;
			case 5 -> AcceleratedFunctions::func_5_OC__Cl;
			case 6 -> AcceleratedFunctions::func_6_RV__Pr;
			case 7 -> AcceleratedFunctions::func_7_OP__Pr;
			case 8 -> AcceleratedFunctions::func_8_CP__Tab;
			case 9 -> AcceleratedFunctions::func_9_RA__Pr;
			case 10 -> AcceleratedFunctions::func_10_RL__Pr;
			case 11 -> AcceleratedFunctions::func_11_OC__Cl;
			case 12 -> AcceleratedFunctions::func_12_RV__Pr;
			case 13 -> AcceleratedFunctions::func_13_OP__Pr;
			default -> {
				LOG.info("Tried to use unknown accelerated function: " + id);
				yield null;
			}
		};
	}

	//Created just because every other function fits the bill for a BiFunction except this one.
	public static int func_1_Z__Region(int address, int ignored) {
		return func_1_Z__Region(address);
	}

	public static int func_1_Z__Region(int address) {
		if (address < 36 || address >= Globals.gluxHeader.END_MEM()) {
			return 0;
		}
		int testByte = Globals.gameMemory.getByte(address) & 0xff;
		if (testByte >= 0xE0) {
			return 3;
		}
		if (testByte >= 0xC0) {
			return 2;
		}
		if (testByte >= 0x70 && testByte <= 0x7F && address >= Globals.gluxHeader.RAM_START()) {
			return 1;
		}
		return 0;
	}

	public static int func_2_CP__Tab(int obj, int id) {
		return CP__Tab_helper(obj, id, 16);
	}

	private static int func_3_RA__Pr(int obj, int id) {
		return RA_RL__PR_helper(obj, id, (prop) -> Globals.gameMemory.getInt(prop + 4), true);
	}

	private static int func_4_RL__Pr(int obj, int id) {
		return RA_RL__PR_helper(obj, id, (prop) -> Globals.gameMemory.getShort(prop + 2) * 4, true);
	}

	private static int func_5_OC__Cl(int obj, int cla) {
		return OC__Cl_helper(obj, cla, true);
	}

	public static int func_6_RV__Pr(int obj, int id) {
		return RV__Pr_helper(obj, id, true);
	}

	private static int func_7_OP__Pr(int obj, int id) {
		return OP__Pr_helper(obj, id, true);
	}

	public static int func_8_CP__Tab(int obj, int id) {
		return CP__Tab_helper(obj, id, ((3 + (PARAM_7_num_attr_bytes / 4)) * 4));
	}

	public static int func_9_RA__Pr(int obj, int id) {
		return RA_RL__PR_helper(obj, id, (prop) -> Globals.gameMemory.getInt(prop + 4), false);
	}

	public static int func_10_RL__Pr(int obj, int id) {
		return RA_RL__PR_helper(obj, id, (prop) -> Globals.gameMemory.getShort(prop + 2) * 4, false);
	}

	public static int func_11_OC__Cl(int obj, int cla) {
		return OC__Cl_helper(obj, cla, false);
	}

	public static int func_12_RV__Pr(int obj, int id) {
		return RV__Pr_helper(obj, id, false);
	}

	public static int func_13_OP__Pr(int obj, int id) {
		return OP__Pr_helper(obj, id, false);
	}

	private static int OP__Pr_helper(int obj, int id, boolean old) {
		int zr = func_1_Z__Region(obj);
		switch (zr) {
			case 3:
				// print = PARAM_1_indiv_prop_start + 6
				// print_to_array = PARAM_1_indiv_prop_start + 7
				return (id == (PARAM_1_indiv_prop_start + 6) || id == (PARAM_1_indiv_prop_start + 7)) ? 1 : 0;
			case 2:
				// call = PARAM_1_indiv_prop_start + 5
				return (id == (PARAM_1_indiv_prop_start + 5)) ? 1 : 0;
			case 1:
				break;
			default:
				return 0;
		}
		if (id >= PARAM_1_indiv_prop_start &&
				id < (PARAM_1_indiv_prop_start + 8) &&
				obj_in_class(obj)) {
			return 1;
		}
		return (old ? func_3_RA__Pr(obj, id) : func_9_RA__Pr(obj, id)) != 0 ? 1 : 0;
	}

	private static int RV__Pr_helper(int obj, int id, boolean old) {
		int addr = old ? func_3_RA__Pr(obj, id) : func_9_RA__Pr(obj, id);
		if (addr == 0) {
			if (id > 0 && id < PARAM_1_indiv_prop_start) {
				return Globals.gameMemory.getInt(PARAM_8_cpv__start + (id * 4));
			}
			LOG.warn("[** Programming error: tried to read (something) **]");
			return 0;
		}
		return Globals.gameMemory.getInt(addr);
	}

	private static int OC__Cl_helper(int obj, int cla, boolean old) {
		int zr = func_1_Z__Region(obj);
		switch (zr) {
			case 1:
				break;
			case 2:
				return cla == PARAM_4_routine_metaclass ? 1 : 0;
			case 3:
				return cla == PARAM_5_string_metaclass ? 1 : 0;
			default:
				return 0;
		}
		if (cla == PARAM_2_class_metaclass) {
			return obj_in_class(obj) ||
					obj == PARAM_2_class_metaclass ||
					obj == PARAM_3_object_metaclass ||
					obj == PARAM_4_routine_metaclass ||
					obj == PARAM_5_string_metaclass ? 1 : 0;
		}
		if (cla == PARAM_3_object_metaclass) {
			return obj_in_class(obj) ||
					obj == PARAM_2_class_metaclass ||
					obj == PARAM_3_object_metaclass ||
					obj == PARAM_4_routine_metaclass ||
					obj == PARAM_5_string_metaclass ? 0 : 1; //flipped from previous
		}
		if (cla == PARAM_4_routine_metaclass || cla == PARAM_5_string_metaclass) {
			return 0;
		}
		if (!obj_in_class(cla)) {
			LOG.warn("[** Programming error: tried to apply 'ofclass' with non-class **]");
			return 0;
		}

		int inlist = old ? func_3_RA__Pr(obj, 2) : func_9_RA__Pr(obj, 2);
		if (inlist == 0) {
			return 0;
		}
		int inlistlen = old ? func_4_RL__Pr(obj, 2) : func_10_RL__Pr(obj, 2);
		for (int i = 0; i < inlistlen; i += 4) {
			if (Globals.gameMemory.getInt(inlist + i) == cla) {
				return 1;
			}
		}
		return 0;
	}

	@SuppressFBWarnings(justification = "Integers are treated as unsigned.", value = {"BIT_SIGNED_CHECK"})
	private static int RA_RL__PR_helper(int obj, int id, Function<Integer, Integer> defaultAction, boolean old) {
		int cla, prop;
		if ((id & 0xffff0000) != 0) {
			cla = Globals.gameMemory.getInt(PARAM_0_classes_table + ((id & 0xffff) * 4));
			if (OC__Cl_helper(obj, cla, old) == 0) {
				return 0;
			}
			id >>>= 16;
			obj = cla;
		} else {
			cla = 0;
		}
		prop = old ? func_2_CP__Tab(obj, id) : func_8_CP__Tab(obj, id);
		if (prop == 0) {
			return 0;
		}
		if (obj_in_class(obj) && cla == 0 && (id < PARAM_1_indiv_prop_start || id >= (PARAM_1_indiv_prop_start + 8))) {
			return 0;
		}
		if (Globals.gameMemory.getInt(PARAM_6_self) != obj && (Globals.gameMemory.getByte(prop + 9) & 1) > 0) {
			return 0;
		}
		return defaultAction.apply(prop);
	}

	private static int CP__Tab_helper(int obj, int id, int offset) {
		if (func_1_Z__Region(obj, 0) != 1) {
			LOG.warn("[** Programming error: tried to find the \".\" of (something) **]");
			return 0;
		}
		int otab = Globals.gameMemory.getInt(obj + offset);
		if (otab == 0) {
			return 0;
		}
		int max = Globals.gameMemory.getInt(otab);
		otab += 4;
		return VMFunctions.binarysearch(id, 2, otab, 10, max, 0, 0);
	}
}
