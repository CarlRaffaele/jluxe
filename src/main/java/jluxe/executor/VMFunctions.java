/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package jluxe.executor;

import annotations.VMFunction;
import blorbReader.*;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import gameMemory.Function;
import gameMemory.GameMemory;
import gameMemory.MemorySegment;
import jluxe.parser.Parser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.IntConsumer;

import static annotations.ParameterType.*;
import static blorbReader.QuetzalChunkType.*;
import static java.lang.Runtime.getRuntime;

@SuppressWarnings("unused")
public class VMFunctions {
	private static final Logger LOG = LogManager.getLogger(VMFunctions.class);
	public static final String SAVED_UNDO_FILE_EXTENSION = ".jluxs";
	// TODO: This would likely be better served as a RAM file is space is available
	private static final Deque<Path> saveUndoFiles = new LinkedList<>();
	private static final int SAVE_UNDO_LIMIT = 5;
	private static boolean ret = false;
	private static int returnValue;
	private static Thread interruptHandlerFunction;

	@VMFunction(code = 0x180, types = {LOAD, LOAD})
	public static void accelfunc(int funcNumber, int address) {
		Globals.parser.enableAcceleratedFunction(address, AcceleratedFunctions.getParsedFunction(funcNumber));
	}

	@VMFunction(code = 0x181, types = {LOAD, LOAD})
	public static void accelparam(int parameterPosition, int value) {
		AcceleratedFunctions.setParameter(parameterPosition, value);
	}

	@VMFunction(code = 0x1B4, types = {LOAD, RETURN}, pure = true)
	public static int acos(int arg) {
		return Float.floatToIntBits((float) Math.acos(Float.intBitsToFloat(arg)));
	}

	@VMFunction(code = 0x10, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int add(int left, int right) {
		return left + right;
	}

	@VMFunction(code = 0x48, types = {LOAD, LOAD, RETURN})
	public static int aload(int address, int offset) {
		return Globals.gameMemory.getInt(address + (offset * 4));
	}

	@VMFunction(code = 0x4B, types = {LOAD, LOAD, RETURN})
	public static int aloadbit(int baseAddress, int index) {
		return ((Globals.gameMemory.getByte(baseAddress + (index >> 3)) & 0xff) & (1 << (index & 7))) > 0 ? 1 : 0;
	}

	@VMFunction(code = 0x49, types = {LOAD, LOAD, RETURN})
	public static int aloads(int baseAddress, int index) {
		int address = baseAddress + (index * 2);
		return Globals.gameMemory.getShort(address) & 0xffff;
	}

	@VMFunction(code = 0x1B3, types = {LOAD, RETURN}, pure = true)
	public static int asin(int arg) {
		return Float.floatToIntBits((float) Math.asin(Float.intBitsToFloat(arg)));
	}

	@VMFunction(code = 0x4C, types = {LOAD, LOAD, LOAD})
	public static void astore(int baseAddress, int index, int value) {
		int address = baseAddress + (index * 4);
		Globals.gameMemory.put(address, value);
	}

	@VMFunction(code = 0x4E, types = {LOAD, LOAD, LOAD})
	public static void astoreb(int baseAddress, int index, int value) {
		Globals.gameMemory.put(baseAddress + index, (byte) value);
	}

	@VMFunction(code = 0x4F, types = {LOAD, LOAD, LOAD})
	public static void astorebit(int baseAddress, int index, int newValue) {
		int address = baseAddress + (index >> 3);
		byte value = (byte) aloadb(address, 0);
		byte mask = (byte) (1 << (index & 7));
		if (newValue == 0) {
			value &= (~mask);
		} else {
			value |= mask;
		}
		Globals.gameMemory.put(address, value);
	}

	@VMFunction(code = 0x4A, types = {LOAD, LOAD, RETURN})
	public static int aloadb(int address, int index) {
		return Globals.gameMemory.getByte(address + index) & 0xff;
	}

	@VMFunction(code = 0x4D, types = {LOAD, LOAD, LOAD})
	public static void astores(int baseAddress, int index, int value) {
		int address = baseAddress + (index * 2);
		Globals.gameMemory.put(address, (char) value);
	}

	@VMFunction(code = 0x1B5, types = {LOAD, RETURN}, pure = true)
	public static int atan(int arg) {
		return Float.floatToIntBits((float) Math.atan(Float.intBitsToFloat(arg)));
	}

	@VMFunction(code = 0x1B6, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int atan2(int y, int x) {
		return Float.floatToIntBits((float) Math.atan2(Float.intBitsToFloat(y), Float.intBitsToFloat(x)));
	}

	@VMFunction(code = 0x151, types = {LOAD, LOAD, LOAD, LOAD, LOAD, LOAD, LOAD, RETURN})
	public static int binarysearch(int key, int keySize, int start, int structSize, int numStructs, int keyOffset,
								   int options) {
		if (keySize <= 8) {
			return longBinarySearchHelper(start, structSize, numStructs, keyOffset, key, keySize, options);
		} else {
			return binarySearchHelper(start, structSize, numStructs, keyOffset, new ByteArraySearchOptions(key, keySize, options), Arrays::compareUnsigned);
		}
	}

	private static int longBinarySearchHelper(int start, int structSize, int numStructs, int keyOffset,
											  int key, int keySize, int options) {
		//Adapted from Arrays.binarySearch
		int low = 0;
		int high = numStructs - 1;
		final long toFind = (options & 1) == 1 ?
				LongSearchOptions.getKey(key, keySize) //indirect key
				: switch (keySize) { //direct key
			case 1 -> key & 0xffL;
			case 2 -> key & 0xffffL;
			case 4 -> key & 0xffffffffL;
			default -> throw new IllegalArgumentException("Illegal keysize: " + keySize);
		};
		final boolean returnIndex = (options & 4) == 4;

		while (low <= high) {
			int mid = (low + high) >>> 1;
			long test = LongSearchOptions.getKey(start + (mid * structSize) + keyOffset, keySize);
			if (test < toFind) {
				low = mid + 1;
			} else if (test > toFind) {
				high = mid - 1;
			} else {
				// key found
				return returnIndex ? // return index?
						mid :
						(start + (mid * structSize));
			}
		}
		return returnIndex ? -1 : 0;  // key not found.
	}

	private static <T> int binarySearchHelper(int start, int structSize, int numStructs, int keyOffset,
											  SearchOptions<T> searchOptions, Comparator<T> comparator) {
		//Adapted from Arrays.binarySearch
		int low = 0;
		int high = numStructs - 1;

		while (low <= high) {
			int mid = (low + high) >>> 1;
			int compare = comparator.compare(searchOptions.getKey(start + (mid * structSize) + keyOffset),
					searchOptions.getKey());
			if (compare < 0) {
				low = mid + 1;
			} else if (compare > 0) {
				high = mid - 1;
			} else {
				return searchOptions.returnIndex() ? mid : (start + (mid * structSize)); // key found
			}
		}
		return searchOptions.getFailValue();  // key not found.
	}

	@VMFunction(code = 0x18, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int bitand(int left, int right) {
		return left & right;
	}

	@VMFunction(code = 0x1B, types = {LOAD, RETURN}, pure = true)
	public static int bitnot(int arg) {
		return ~arg;
	}

	@VMFunction(code = 0x19, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int bitor(int left, int right) {
		return left | right;
	}

	@VMFunction(code = 0x1A, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int bitxor(int left, int right) {
		return left ^ right;
	}

	private static void call(int address) {
		callf(address, LoadStoreModeImpl.ZERO, 0);
	}

	@VMFunction(code = 0x30, types = {LOAD, LOAD, STORE})
	public static void call(int address, int numArgs, @NotNull LoadStoreMode storeMode, int storeAddress) {
		switch (numArgs) {
			case 0 -> callf(address, storeMode, storeAddress);
			case 1 -> callfi(address, Globals.gameMemory.getCurrentCallFrame().pop(), storeMode, storeAddress);
			case 2 -> callfii(address,
					Globals.gameMemory.getCurrentCallFrame().pop(),
					Globals.gameMemory.getCurrentCallFrame().pop(),
					storeMode,
					storeAddress);
			case 3 -> callfiii(address,
					Globals.gameMemory.getCurrentCallFrame().pop(),
					Globals.gameMemory.getCurrentCallFrame().pop(),
					Globals.gameMemory.getCurrentCallFrame().pop(),
					storeMode,
					storeAddress);
			default -> call(address, Globals.gameMemory.getCurrentCallFrame().pop(numArgs), storeMode, storeAddress);
		}
	}

	@VMFunction(code = 0x160, types = {LOAD, STORE})
	public static void callf(int address, @NotNull LoadStoreMode storeMode, int storeAddress) {
		Globals.gameMemory.getCurrentCallFrame().pushCallStub(storeMode.toCallStubType(), storeAddress);
		Function pf = Globals.parser.parseFunction(address);
		Globals.gameMemory.pushCallFrameFromFunction(pf);
		callWorker();
	}

	@VMFunction(code = 0x161, types = {LOAD, LOAD, STORE})
	public static void callfi(int address, int arg1, @NotNull LoadStoreMode storeMode, int storeAddress) {
		Globals.gameMemory.getCurrentCallFrame().pushCallStub(storeMode.toCallStubType(), storeAddress);
		Function pf = Globals.parser.parseFunction(address);
		Globals.gameMemory.pushCallFrameFromFunction(pf, arg1);
		if (pf.isAccelerated()) {
			//Possible error/warning? Only 1 accelerated function can have 1 argument
			Globals.gameMemory.popCallFrame(pf.runAcceleratedFunction(arg1, 0));
		} else {
			callWorker();
		}
	}

	@VMFunction(code = 0x162, types = {LOAD, LOAD, LOAD, STORE})
	public static void callfii(int address, int arg1, int arg2, @NotNull LoadStoreMode storeMode, int storeAddress) {
		Globals.gameMemory.getCurrentCallFrame().pushCallStub(storeMode.toCallStubType(), storeAddress);
		Function pf = Globals.parser.parseFunction(address);
		Globals.gameMemory.pushCallFrameFromFunction(pf, arg1, arg2);
		if (pf.isAccelerated()) {
			Globals.gameMemory.popCallFrame(pf.runAcceleratedFunction(arg1, arg2));
		} else {
			callWorker();
		}
	}

	@VMFunction(code = 0x163, types = {LOAD, LOAD, LOAD, LOAD, STORE})
	public static void callfiii(int address, int arg1, int arg2, int arg3, @NotNull LoadStoreMode storeMode, int storeAddress) {
		Globals.gameMemory.getCurrentCallFrame().pushCallStub(storeMode.toCallStubType(), storeAddress);
		Function pf = Globals.parser.parseFunction(address);
		Globals.gameMemory.pushCallFrameFromFunction(pf, arg1, arg2, arg3);
		if (pf.isAccelerated()) {
			Globals.gameMemory.popCallFrame(pf.runAcceleratedFunction(arg1, arg2));
		} else {
			callWorker();
		}
	}

	public static void call(int address, int[] args, @NotNull LoadStoreMode storeMode, int storeAddress) {
		Globals.gameMemory.getCurrentCallFrame().pushCallStub(storeMode.toCallStubType(), storeAddress);
		Function pf = Globals.parser.parseFunction(address);
		Globals.gameMemory.pushCallFrameFromFunction(pf, args);
		if (pf.isAccelerated()) {
			if (args.length >= 2) {
				Globals.gameMemory.popCallFrame(pf.runAcceleratedFunction(args[0], args[1]));
			} else if (args.length == 1) {
				//This should only be used for func_1_Z__Region, because it has 1 argument
				Globals.gameMemory.popCallFrame(pf.runAcceleratedFunction(args[0], 0));
			} else {
				LOG.warn("Not enough arguments to accelerated function: " + Arrays.toString(args));
				Globals.gameMemory.popCallFrame(0);
			}
		} else {
			callWorker();
		}
	}

	private static void callWorker() {
		while (!ret) {
			Globals.parser.parseOP(Globals.gameMemory).run();
		}
		ret = false;
		Globals.gameMemory.popCallFrame(returnValue);
	}

	@VMFunction(code = 0x198, types = {LOAD, RETURN})
	public static int ceil(int arg) {
		return Float.floatToIntBits((float) Math.ceil(Float.intBitsToFloat(arg)));
	}

	@VMFunction(code = 0x42, types = {LOAD_BYTE, RETURN_BYTE})
	@VMFunction(code = 0x41, types = {LOAD_SHORT, RETURN_SHORT})
	@VMFunction(code = 0x40, types = {LOAD, RETURN})
	public static int copy(int arg) {
		return arg;
	}

	@VMFunction(code = 0x1B1, types = {LOAD, RETURN}, pure = true)
	public static int cos(int arg) {
		return Float.floatToIntBits((float) Math.cos(Float.intBitsToFloat(arg)));
	}

	@VMFunction(code = 0x101, types = {LOAD})
	public static void debugtrap(int arg) {
		System.err.println("Debugtrap called with value: " + arg);
	}

	@VMFunction(code = 0x13, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int div(int left, int right) {
		return left / right;
	}

	@VMFunction(code = 0x1A9, types = {LOAD, RETURN}, pure = true)
	public static int exp(int arg) {
		return Float.floatToIntBits((float) Math.exp(Float.intBitsToFloat(arg)));
	}

	@VMFunction(code = 0x1A0, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int fadd(int left, int right) {
		return Float.floatToIntBits(Float.intBitsToFloat(left) + Float.intBitsToFloat(right));
	}

	@VMFunction(code = 0x1A3, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int fdiv(int left, int right) {
		return Float.floatToIntBits(Float.intBitsToFloat(left) / Float.intBitsToFloat(right));
	}

	@VMFunction(code = 0x199, types = {LOAD, RETURN})
	public static int floor(int arg) {
		return Float.floatToIntBits((float) Math.floor(Float.intBitsToFloat(arg)));
	}

	@VMFunction(code = 0x1A4, types = {LOAD, LOAD, STORE, STORE})
	public static void fmod(int divisor, int dividend,
							LoadStoreMode moduloMode, int moduloAddress,
							LoadStoreMode quotientMode, int quotientAddress) {
		float l1 = Float.intBitsToFloat(divisor);
		float l2 = Float.intBitsToFloat(dividend);
		float mod = l1 % l2;
		float div = (l1 - mod) / l2;
		if (div == 0.0f || div == -0.0f) {
			div = (Float.floatToRawIntBits(l1) & 0x80000000) == (Float.floatToRawIntBits(l2) & 0x80000000) ? 0.0f : -0.0f;
		}
		moduloMode.storeAsInt(moduloAddress, Float.floatToIntBits(mod));
		quotientMode.storeAsInt(quotientAddress, Float.floatToIntBits(div));
	}

	@VMFunction(code = 0x1A2, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int fmul(int left, int right) {
		return Float.floatToIntBits(Float.intBitsToFloat(left) * Float.intBitsToFloat(right));
	}

	@VMFunction(code = 0x1A1, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int fsub(int left, int right) {
		return Float.floatToIntBits(Float.intBitsToFloat(left) - Float.intBitsToFloat(right));
	}

	@VMFunction(code = 0x192, types = {LOAD, RETURN})
	public static int ftonumn(int f) {
		float f1 = Float.intBitsToFloat(f);
		int ret = Math.round(f1);
		if (Float.isNaN(f1) && f >> 31 == 0) {
			ret = Integer.MAX_VALUE;
		}
		if (Float.isNaN(f1) && f >> 31 == -1) {
			ret = Integer.MIN_VALUE;
		}
		return ret;
	}

	@VMFunction(code = 0x191, types = {LOAD, RETURN})
	public static int ftonumz(int f) {
		float f1 = Float.intBitsToFloat(f);
		int ret = (int) f1;
		if (f1 == Float.POSITIVE_INFINITY || (Float.isNaN(f1) && f >> 31 == 0) || f1 > Integer.MAX_VALUE) {
			ret = Integer.MAX_VALUE;
		}
		if (f1 == Float.NEGATIVE_INFINITY || (Float.isNaN(f1) && f >> 31 == -1) || f1 < Integer.MIN_VALUE) {
			ret = Integer.MIN_VALUE;
		}
		return ret;
	}

	@VMFunction(code = 0x100, types = {LOAD, LOAD, RETURN})
	@SuppressFBWarnings({"DB_DUPLICATE_SWITCH_CLAUSES"})
	public static int gestalt(int selector, int argument) {
		return switch (selector) {
			case 0 -> 0x00030103;// Glux Spec version used
			case 1 -> 1; // Interpreter version
			case 2 -> 1; // Can resize memory
			case 3 -> 1; // Supports undo
			case 4 -> argument <= 2 ? 1 : 0; // Supports specific I/O (in argument)
			case 5 -> 1; // Supports Unicode
			case 6 -> 1; // Support Memcopy
			case 7 -> 0; // Supports MAlloc
			case 8 -> 0; // Supports MAlloc Heap
			case 9 -> 1; // Supports acceleration
			case 10 -> // Supports specific acceleration
					AcceleratedFunctions.getParsedFunction(argument) == null ? 0 : 1;
			case 11 -> 1; // Supports float
			default -> {
				LOG.info("Unknown gestalt value: " + selector);
				yield 0;
			}
		};
	}

	@VMFunction(code = 0x148, types = {STORE, STORE})
	public static void getiosys(@NotNull LoadStoreMode systemStore, int systemStoreAddress,
								@NotNull LoadStoreMode rockStore, int rockStoreAddress) {
		systemStore.storeAsInt(systemStoreAddress, Globals.currentIOSystem);
		rockStore.storeAsInt(rockStoreAddress, Globals.IOSystemRock);
	}

	@VMFunction(code = 0x102, types = {RETURN})
	public static int getmemsize() {
		return Globals.gameMemory.getSize();
	}

	@VMFunction(code = 0x130, types = {LOAD, LOAD, RETURN})
	public static int glk(int identifier, int numArgs) {
		if (identifier == 0x2) { // glk_set_interrupt_handler
			Globals.gameMemory.getCurrentCallFrame().pop(); // only one value
			int address = Globals.gameMemory.getCurrentCallFrame().pop();
			synchronized (VMFunctions.class) {
				if (interruptHandlerFunction != null) {
					getRuntime().removeShutdownHook(interruptHandlerFunction);
				}
				if (address <= 0) {
					interruptHandlerFunction = new Thread(() -> call(address));
					getRuntime().addShutdownHook(interruptHandlerFunction);
				}
			}
			return 0;
		} else {
			int[] gargs = new int[numArgs];
			for (int i = 0; i < numArgs; i++) {
				gargs[i] = Globals.gameMemory.getCurrentCallFrame().pop();
			}
			try {
				return Globals.glk.glk(identifier, gargs);
			} catch (Exception e) {
				LOG.error("Error running GLK function: " + e.toString(), e);
				return 0;
			}
		}
	}

	@VMFunction(code = 0x24, types = {LOAD, LOAD, LOAD})
	public static void jeq(int left, int right, int offset) {
		if (left == right) {
			performJump(offset);
		}
	}

	private static void performJump(int offset) {
		if (offset == 0 || offset == 1) {
			ret(offset);
		}
		Globals.gameMemory.position(offset - 2 + Globals.gameMemory.position());
	}

	@VMFunction(code = 0x104, types = {LOAD})
	public static void jumpabs(int address) {
		if (address == 0 || address == 1) {
			ret(address);
		}
		Globals.gameMemory.position(address);
	}

	@VMFunction(code = 0x1C0, types = {LOAD, LOAD, LOAD, LOAD})
	public static void jfeq(int left, int right, int epsilon, int offset) {
		if (feq(left, right, epsilon)) {
			performJump(offset);
		}
	}

	private static boolean feq(int left, int right, int epsilon) {
		float fleft = Float.intBitsToFloat(left);
		float fright = Float.intBitsToFloat(right);
		float fepsilon = Float.intBitsToFloat(epsilon);
		if (Float.isNaN(fleft) || Float.isNaN(fright) || Float.isNaN(fepsilon)) {
			return false;
		}
		if (Float.isInfinite(fleft) && Float.isInfinite(fright)) {
			return fleft == fright;
		} else {
			return fepsilon == 0 && fleft == fright || Math.abs(fleft - fright) <= Math.abs(fepsilon);
		}
	}

	@VMFunction(code = 0x1C5, types = {LOAD, LOAD, LOAD})
	public static void jfge(int left, int right, int offset) {
		if (Float.intBitsToFloat(left) >= Float.intBitsToFloat(right)) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x1C4, types = {LOAD, LOAD, LOAD})
	public static void jfgt(int left, int right, int offset) {
		if (Float.intBitsToFloat(left) > Float.intBitsToFloat(right)) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x1C3, types = {LOAD, LOAD, LOAD})
	public static void jfle(int left, int right, int offset) {
		if (Float.intBitsToFloat(left) <= Float.intBitsToFloat(right)) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x1C2, types = {LOAD, LOAD, LOAD})
	public static void jflt(int left, int right, int offset) {
		if (Float.intBitsToFloat(left) < Float.intBitsToFloat(right)) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x1C1, types = {LOAD, LOAD, LOAD, LOAD})
	public static void jfne(int left, int right, int epsilon, int offset) {
		if (!feq(left, right, epsilon)) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x27, types = {LOAD, LOAD, LOAD})
	public static void jge(int left, int right, int offset) {
		if (left >= right) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x2B, types = {LOAD, LOAD, LOAD})
	public static void jgeu(int left, int right, int offset) {
		if (Integer.compareUnsigned(left, right) >= 0) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x28, types = {LOAD, LOAD, LOAD})
	public static void jgt(int left, int right, int offset) {
		if (left > right) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x2C, types = {LOAD, LOAD, LOAD})
	public static void jgtu(int left, int right, int offset) {
		if (Integer.compareUnsigned(left, right) > 0) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x1C9, types = {LOAD, LOAD})
	public static void jisinf(int value, int offset) {
		if (Float.isInfinite(Float.intBitsToFloat(value))) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x1C8, types = {LOAD, LOAD})
	public static void jisnan(int value, int offset) {
		if (Float.isNaN(Float.intBitsToFloat(value))) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x29, types = {LOAD, LOAD, LOAD})
	public static void jle(int left, int right, int offset) {
		if (left <= right) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x2D, types = {LOAD, LOAD, LOAD})
	public static void jleu(int left, int right, int offset) {
		if (Integer.compareUnsigned(left, right) <= 0) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x26, types = {LOAD, LOAD, LOAD})
	public static void jlt(int left, int right, int offset) {
		if (left < right) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x2A, types = {LOAD, LOAD, LOAD})
	public static void jltu(int left, int right, int offset) {
		if (Integer.compareUnsigned(left, right) < 0) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x25, types = {LOAD, LOAD, LOAD})
	public static void jne(int left, int right, int offset) {
		if (left != right) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x23, types = {LOAD, LOAD})
	public static void jnz(int value, int offset) {
		if (value != 0) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x20, types = {LOAD})
	public static void jump(int offset) {
		performJump(offset);
	}

	@VMFunction(code = 0x32, types = {STORE, LOAD})
	public static void opcatch(int offset, @NotNull LoadStoreMode store, int storeAddress) {
		Globals.gameMemory.getCurrentCallFrame().pushCallStub(store.toCallStubType(), storeAddress);
		performJump(offset);
		store.storeAsInt(storeAddress, Globals.gameMemory.getCurrentCallFrame().getStackPointer());
	}

	@VMFunction(code = 0x22, types = {LOAD, LOAD})
	public static void jz(int value, int offset) {
		if (value == 0) {
			performJump(offset);
		}
	}

	@VMFunction(code = 0x150, types = {LOAD, LOAD, LOAD, LOAD, LOAD, LOAD, LOAD, RETURN})
	public static int linearsearch(int key, int keySize, int start, int structSize, int numStructs, int keyOffset,
								   int options) {
		if (keySize <= 8) {
			return linearSearchHelper(new LongSearchOptions(key, keySize, options), start, structSize, numStructs,
					keyOffset);
		} else {
			return linearSearchHelper(new ByteArraySearchOptions(key, keySize, options), start, structSize, numStructs,
					keyOffset);
		}
	}

	private static <T> int linearSearchHelper(SearchOptions<T> searchOptions, int start, int structSize,
											  int numStructs, int keyOffset) {
		for (int address = start + keyOffset, index = 0; numStructs == -1 || index < numStructs; address += structSize, index += 1) {
			T test = searchOptions.getKey(address);
			if (searchOptions.keyMatches(test)) {
				return searchOptions.returnIndex() ? index : (address - keyOffset);
			} else if (searchOptions.zeroKeyTerminates() && searchOptions.isZeroKey(test)) {
				break;
			}
		}
		return searchOptions.getFailValue();
	}

	@VMFunction(code = 0x152, types = {LOAD, LOAD, LOAD, LOAD, LOAD, LOAD, RETURN})
	public static int linkedsearch(int key, int keySize, int start, int keyOffset, int nextOffset, int options) {
		if (keySize <= 8) {
			return linkedSearchHelper(new LongSearchOptions(key, keySize, options), start, keyOffset, nextOffset);
		} else {
			return linkedSearchHelper(new ByteArraySearchOptions(key, keySize, options), start, keyOffset, nextOffset);
		}
	}

	private static <T> int linkedSearchHelper(SearchOptions<T> searchOptions, int start, int keyOffset, int nextOffset) {
		for (int address = start; address != 0; address = Globals.gameMemory.getInt(address + nextOffset)) {
			T test = searchOptions.getKey(address + keyOffset);
			if (searchOptions.keyMatches(test)) {
				return address;
			} else if (searchOptions.zeroKeyTerminates() && searchOptions.isZeroKey(test)) {
				break;
			}
		}
		return searchOptions.getFailValue();
	}

	@VMFunction(code = 0x1AA, types = {LOAD, RETURN}, pure = true)
	public static int log(int arg) {
		return Float.floatToIntBits((float) Math.log(Float.intBitsToFloat(arg)));
	}

	@VMFunction(code = 0x178, types = {LOAD, RETURN})
	public static int malloc(int size) {
		return Globals.gameMemory.malloc(size);
	}

	@VMFunction(code = 0x171, types = {LOAD, LOAD, LOAD})
	public static void mcopy(int numBytes, int fromAddress, int toAddress) {
		Globals.gameMemory.copy(fromAddress, toAddress, numBytes);
	}

	@VMFunction(code = 0x179, types = {LOAD})
	public static void mfree(int address) {
		Globals.gameMemory.free(address);
	}

	@VMFunction(code = 0x14, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int mod(int left, int right) {
		return left % right;
	}

	@VMFunction(code = 0x12, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int mul(int left, int right) {
		return left * right;
	}

	@VMFunction(code = 0x170, types = {LOAD, LOAD})
	public static void mzero(int length, int address) {
		Globals.gameMemory.zero(address, length);
	}

	@VMFunction(code = 0x15, types = {LOAD, RETURN}, pure = true)
	public static int neg(int arg) {
		return -arg;
	}

	@SuppressWarnings({"unused", "EmptyMethod"})
	@VMFunction(code = 0, pure = true)
	public static void nop() {
	}

	@VMFunction(code = 0x190, types = {LOAD, RETURN})
	public static int numtof(int num) {
		return Float.floatToIntBits((float) num);
	}

	@VMFunction(code = 0x33, types = {LOAD, LOAD})
	public static void opthrow(int storeValue, int tokenAddress) {
		while (Globals.gameMemory.getCurrentCallFrame().getFramePtr() > tokenAddress) { // pop down to the right cf
			Globals.gameMemory.popCallFrame();
		}
		Globals.gameMemory.getCurrentCallFrame().setStackPointer(tokenAddress);
		Globals.gameMemory.getCurrentCallFrame().popCallStubAndStoreValue(storeValue);
	}

	@VMFunction(code = 0x1Ab, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int pow(int left, int right) {
		float l1 = Float.intBitsToFloat(left);
		float l2 = Float.intBitsToFloat(right);
		if (l1 == 1.0f) {
			return Float.floatToIntBits(l1);
		}
		if (l1 == -1.0f && Float.isInfinite(l2)) {
			return Float.floatToIntBits(1.0f);
		}
		return Float.floatToIntBits((float) Math.pow(l1, l2));
	}

	@VMFunction(code = 0x127, types = {LOAD, LOAD})
	public static void protect(int startAddress, int length) {
		Globals.gameMemory.protect(startAddress, length);
	}

	@VMFunction(code = 0x120)
	@SuppressFBWarnings(justification = "That's the point of quit.", value = {"DM_EXIT"})
	public static void quit() {
		System.exit(0);
	}

	@VMFunction(code = 0x110, types = {LOAD, RETURN})
	public static int random(int limit) {
		if (limit == 0) {
			return Globals.rng.nextInt();
		} else if (limit > 0) {
			return Globals.rng.nextInt(limit);
		} else {
			return -Globals.rng.nextInt(-limit);
		}
	}

	@VMFunction(code = 0x122)
	public static void restart() {
		throw new RestartException();
	}

	@VMFunction(code = 0x124, types = {LOAD, RETURN})
	public static int restore(int stream) {
		LOG.debug("Restoring");
		throw new RestoreException(Globals.glk.restoreGame(stream));
	}

	@VMFunction(code = 0x126, types = {RETURN})
	@SuppressWarnings("SameReturnValue")
	public static int restoreundo() {
		LOG.debug("Restore-undoing");
		if (saveUndoFiles.isEmpty()) {
			return 1;
		}
		throw new RestoreException(new RestorePackage(new QuetzalFileReader(saveUndoFiles.pop())));
	}

	@VMFunction(code = 0x31, types = {LOAD})
	public static void ret(int returnValue) {
		ret = true;
		VMFunctions.returnValue = returnValue;
	}

	@VMFunction(code = 0x123, types = {LOAD, STORE})
	public static void save(int stream, @NotNull LoadStoreMode store, int storeAddress) {
		Globals.gameMemory.getCurrentCallFrame().pushCallStub(store.toCallStubType(), storeAddress);
		int returnValue = Globals.glk.saveGame(stream, Globals.gameMemory) ? 0 : 1;
		Globals.gameMemory.getCurrentCallFrame().popCallStubAndStoreValue(returnValue);
	}

	@VMFunction(code = 0x125, types = {STORE})
	public static void saveundo(LoadStoreMode store, int storeAddress) {
		Globals.gameMemory.getCurrentCallFrame().pushCallStub(store.toCallStubType(), storeAddress);
		Path saveUndoFile;
		try {
			saveUndoFile = Files.createTempFile("", SAVED_UNDO_FILE_EXTENSION);
		} catch (IOException e) {
			System.err.println("Unable to create saveundo file");
			e.printStackTrace();
			Globals.gameMemory.getCurrentCallFrame().popCallStubAndStoreValue(1);
			return;
		}
		saveUndoFiles.push(saveUndoFile);
		if (saveUndoFiles.size() > SAVE_UNDO_LIMIT) {
			try {
				Files.deleteIfExists(saveUndoFiles.removeLast());
			} catch (IOException ignored) {
			}
		}

		QuetzalFileWriter writer = new QuetzalFileWriter(saveUndoFile);
		writer.addChunk(UNCOMPRESSED_MEMORY, Globals.gameMemory.asReadOnly().position(0));
		writer.addChunk(IF_HEADER, Globals.gameMemory.slice(0, 128));
		writer.addChunk(STACKS,
				Globals.gameMemory.getStack().slice(0, Globals.gameMemory.getCurrentCallFrame().getStackPointer()));
		try {
			writer.write();
		} catch (IOException e) {
			System.err.println("Unable to save game");
			e.printStackTrace();
			Globals.gameMemory.getCurrentCallFrame().popCallStubAndStoreValue(1);
			return;
		}
		Globals.gameMemory.getCurrentCallFrame().popCallStubAndStoreValue(0);
	}

	@VMFunction(code = 0x149, types = {LOAD, LOAD})
	public static void setiosys(int system, int rock) {
		Globals.currentIOSystem = system;
		Globals.IOSystemRock = rock;
		switch (Globals.currentIOSystem) {
			case 1 -> Globals.IOSystemStream = filterConsumerGenerator(Globals.IOSystemRock);
			case 2 -> Globals.IOSystemStream = Globals.glk::writeChar;
			default -> Globals.IOSystemStream = VMFunctions::doNothingConsumer;
		}
	}

	@Contract(pure = true)
	private static @NotNull IntConsumer filterConsumerGenerator(int functionAddress) {
		return codepoint -> callfi(functionAddress, codepoint, LoadStoreModeImpl.ZERO, 0);
	}

	@SuppressWarnings("EmptyMethod")
	private static void doNothingConsumer(int ignored) {
	}

	@VMFunction(code = 0x103, types = {LOAD, RETURN})
	public static int setmemsize(int newSize) {
		if (newSize < Globals.gluxHeader.END_MEM()) {
			return 1;
		}
		try {
			Globals.gameMemory.resize(newSize);
		} catch (Exception e) {
			return 1;
		}
		return 0;
	}

	@VMFunction(code = 0x111, types = {LOAD})
	public static void setrandom(int seed) {
		Globals.rng.setSeed(seed);
	}

	@VMFunction(code = 0x45, types = {LOAD, RETURN}, pure = true)
	public static int sexb(int value) {
		return (byte) value;
	}

	@VMFunction(code = 0x44, types = {LOAD, RETURN}, pure = true)
	public static int sexs(int value) {
		return (short) value;
	}

	@VMFunction(code = 0x1C, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int shiftl(int base, int shiftAmount) {
		return (shiftAmount < 0 || shiftAmount >= 32) ? 0 : base << shiftAmount;
	}

	@VMFunction(code = 0x1B0, types = {LOAD, RETURN}, pure = true)
	public static int sin(int arg) {
		return Float.floatToIntBits((float) Math.sin(Float.intBitsToFloat(arg)));
	}

	@VMFunction(code = 0x1A8, types = {LOAD, RETURN}, pure = true)
	public static int sqrt(int arg) {
		return Float.floatToIntBits((float) Math.sqrt(Float.intBitsToFloat(arg)));
	}

	@VMFunction(code = 0x1D, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int sshiftr(int base, int shiftAmount) {
		return (shiftAmount < 0 || shiftAmount >= 32) ? (base < 0 ? -1 : 0) : base >> shiftAmount;
	}

	public static void start(MemorySegment gameData, GlulxHeader gluxHeader,
							 GlulxDebuggingHeader debuggingHeader, @Nullable BlorbReader blorb) {

		getRuntime().addShutdownHook(new Thread(() -> {
			for (Path saveUndoFile : saveUndoFiles) {
				try {
					Files.deleteIfExists(saveUndoFile);
				} catch (IOException ignored) {
				}
			}
		}));
		Globals.glk.setGameBlorb(blorb);
		Globals.originalGameFile = MemorySegment.fromSize(gluxHeader.EXT_START())
												.put(gameData.rewind())
												.rewind()
												.asReadOnly();
		Globals.gameMemory = GameMemory.fromSizes(gluxHeader.END_MEM(), gluxHeader.STACK_SIZE());
		Globals.glk.setGameMemory(Globals.gameMemory);

		boolean restart;
		RestorePackage restorePackage = null;
		do {
			Globals.gluxHeader = gluxHeader;
			Globals.gluxDebuggingHeader = debuggingHeader;
			int protectStart = Globals.gameMemory.protectedAddress();
			byte[] protectedValues;
			if (protectStart + Globals.gameMemory.protectedLength() < Globals.gameMemory.length()) {
				protectedValues = Globals.gameMemory.getProtectedValues();
			} else {
				//The memory shrank after the save and the values were lost
				protectedValues = new byte[Globals.gameMemory.protectedLength()];
			}

			if (restorePackage == null) { // Starting/restarting a game
				Globals.gameMemory.initializeFrom(Globals.originalGameFile.rewind(), gluxHeader.END_MEM());
				Globals.parser = new Parser((GameMemory) Globals.gameMemory.position(Globals.gluxHeader.START_FUNCTION_ADDRESS()));
				if (verify() != 0) {
					LOG.warn("Game checksum does not match");
				}

				Globals.gameMemory.pushCallFrameFromFunction(
						Globals.parser.parseFunction(Globals.gluxHeader.START_FUNCTION_ADDRESS()),
						new int[0]
				);
			} else { // Undoing a game
				MemorySegment stack = MemorySegment.fromSize(restorePackage.stks().length())
												   .put(restorePackage.stks())
												   .rewind();
				MemorySegment mem;
				if (restorePackage.uMem() != null) {
					mem = restorePackage.uMem();
				} else if (restorePackage.cMem() != null) {
					throw new UnsupportedOperationException();
//					mem = MemorySegment.fromSize(gluxHeader.END_MEM());
//					CMemHandler.decompressSave(Globals.originalGameFile, restorePackage.cMem(), mem);
				} else {
					throw new IllegalArgumentException("Tried to restore a game without a memory segment");
				}
				mem.rewind();
				Globals.parser = new Parser((GameMemory) Globals.gameMemory.position(Globals.gluxHeader.START_FUNCTION_ADDRESS()));
				Globals.gameMemory.initializeFrom(mem, stack);
				Globals.gameMemory.put(protectStart, protectedValues);

				restorePackage = null;
			}

			Globals.gameMemory.put(protectStart, protectedValues);
			Globals.parser.setDecodingTable(gluxHeader.DECODING_TABLE_ADDRESS());

			restart = false;
			ret = false;
			try {
				do {
					// On a restore the Java call stack will start from the topmost level, this loop continues
					// running the program until all the game callframes are exhausted.
					callWorker();
				} while (Globals.gameMemory.hasCallFrame());
			} catch (RestartException e) {
				restart = true;
			} catch (RestoreException e) {
				restorePackage = e.restorePackage;
				restart = true;
			}
		} while (restart);
	}

	@VMFunction(code = 0x121, types = {RETURN})
	public static int verify() {
		MemorySegment test = Globals.originalGameFile.rewind();
		int sum = 0;
		while (test.hasRemaining()) {
			sum += test.getInt();
		}
		sum -= Globals.gluxHeader.CHECKSUM();
		return sum == Globals.gluxHeader.CHECKSUM() ? 0 : 1;
	}

	@VMFunction(code = 0x54, types = {LOAD})
	public static void stkcopy(int count) {
		int[] values = new int[count];

		for (int i = count - 1; i >= 0; i--) {
			values[i] = Globals.gameMemory.getCurrentCallFrame().pop();
		}
		Arrays.stream(values).forEach(Globals.gameMemory.getCurrentCallFrame()::push);
		Arrays.stream(values).forEach(Globals.gameMemory.getCurrentCallFrame()::push);
	}

	@VMFunction(code = 0x50, types = {RETURN})
	public static int stkcount() {
		return Globals.gameMemory.getCurrentCallFrame().stackCount();
	}

	@VMFunction(code = 0x51, types = {LOAD, RETURN})
	public static int stkpeek(int depth) {
		return Globals.gameMemory.getCurrentCallFrame().stackPeek(depth);
	}

	@VMFunction(code = 0x52)
	public static void stkswap() {
		stkroll(2, 1);
	}

	@VMFunction(code = 0x53, types = {LOAD, LOAD})
	public static void stkroll(int numRolled, int distance) {
		if (numRolled == 0) {
			return;
		}
		int[] values = new int[numRolled];

		for (int i = numRolled - 1; i >= 0; i--) {
			values[i] = Globals.gameMemory.getCurrentCallFrame().pop();
		}
		Stack<Integer> vals = new Stack<>();
		for (int v : values) {
			vals.push(v);
		}

		if (distance > 0) {
			for (int i = 0; i < distance; i++) {
				vals.add(0, vals.pop());
			}
		} else if (distance < 0) {
			for (int i = 0; i < -distance; i++) {
				vals.push(vals.remove(0));
			}
		}
		vals.forEach(Globals.gameMemory.getCurrentCallFrame()::push);
	}

	@VMFunction(code = 0x73, types = {LOAD})
	@VMFunction(code = 0x70, types = {LOAD})
	public static void streamchar(int codePoint) {
		Globals.IOSystemStream.accept(codePoint);
	}

	@VMFunction(code = 0x71, types = {LOAD})
	public static void streamnum(int number) {
		Integer.toString(number).chars().forEachOrdered(VMFunctions::streamchar);
	}

	@VMFunction(code = 0x72, types = {LOAD})
	public static void streamstr(int address) {
		Globals.parser.parseAndStreamString(address);
	}

	@VMFunction(code = 0x11, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int sub(int left, int right) {
		return left - right;
	}

	@VMFunction(code = 0x34, types = {LOAD, LOAD})
	public static void tailcall(int address, int numArgs) {
		int[] tailCallArgs = Globals.gameMemory.getCurrentCallFrame().pop(numArgs);
		Globals.gameMemory.popCallFrame();
		Globals.gameMemory.pushCallFrameFromFunction(Globals.parser.parseFunction(address), tailCallArgs);
	}

	@VMFunction(code = 0x1B2, types = {LOAD, RETURN}, pure = true)
	public static int tan(int arg) {
		return Float.floatToIntBits((float) Math.tan(Float.intBitsToFloat(arg)));
	}

	@VMFunction(code = 0x1E, types = {LOAD, LOAD, RETURN}, pure = true)
	public static int ushiftr(int base, int shiftAmount) {
		return (shiftAmount < 0 || shiftAmount >= 32) ? 0 : base >>> shiftAmount;
	}

	@VMFunction(code = 0x128, types = {RETURN})
	public static int hasundo() {
		return saveUndoFiles.isEmpty() ? 0 : 1;
	}

	@VMFunction(code = 0x129)
	public static void discardundo() {
		if (!saveUndoFiles.isEmpty()) {
			saveUndoFiles.pop();
		}
	}

	@VMFunction(code = 0x140, types = {RETURN})
	public static int getDecodingTableAddress() {
		return Globals.parser.getDecodingTableAddress();
	}

	@VMFunction(code = 0x141, types = {LOAD})
	public static void setDecodingTable(int address) {
		Globals.parser.setDecodingTable(address);
	}

	/**
	 * {@link ExitException} and its decendents are control flow exceptions (yes, I know), that unwind the call stack
	 * back to the original start function call.
	 */
	private static class ExitException extends RuntimeException {
	}

	private static class RestartException extends ExitException {
	}

	@SuppressFBWarnings(justification = "Don't serialize this exception", value = {"SE_BAD_FIELD"})
	private static class RestoreException extends ExitException {
		private final RestorePackage restorePackage;

		private RestoreException(RestorePackage restorePackage) {
			this.restorePackage = restorePackage;
		}
	}

}
