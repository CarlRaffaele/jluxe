/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package jluxe.executor;

import blorbReader.GlulxDebuggingHeader;
import blorbReader.GlulxHeader;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import gameMemory.GameMemory;
import gameMemory.MemorySegment;
import jluxe.parser.Parser;

import java.util.Random;
import java.util.function.IntConsumer;

/**
 * Static class to keep track of the game's global constants, along with helper functions.
 */
@SuppressFBWarnings
public class Globals {
	public static GameMemory gameMemory;
	final static Random rng;
	public static jluxeToGLK glk;
	static Parser parser;
	static GlulxHeader gluxHeader;
	static GlulxDebuggingHeader gluxDebuggingHeader;
	public static IntConsumer IOSystemStream = ignored -> {
	};
	static MemorySegment originalGameFile;
	static int currentIOSystem = 0;
	static int IOSystemRock = 0;

	static {
		rng = new Random();
	}

}
