/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package jluxe.executor;

public class LongSearchOptions implements SearchOptions<Long> {
	private final long key;
	private final int keySize;
	private final boolean zeroKeyTerminates;
	private final boolean returnIndex;

	public LongSearchOptions(int key, int keySize, int options) {
		this.keySize = keySize;
		zeroKeyTerminates = (options & 2) == 2;
		returnIndex = (options & 4) == 4;
		this.key = (options & 1) == 1 ?
				getKey(key) //indirect key
				: switch (keySize) { //direct key
			case 1 -> key & 0xffL;
			case 2 -> key & 0xffffL;
			case 4 -> key & 0xffffffffL;
			default -> throw new IllegalArgumentException("Illegal keysize: " + keySize);
		};
	}

	@Override
	public boolean isZeroKey(Long key) {
		return key == 0;
	}

	@Override
	public int getFailValue() {
		return this.returnIndex ? -1 : 0;
	}

	@Override
	public Long getKey() {
		return key;
	}

	@Override
	public Long getKey(int address) {
		return getKey(address, keySize);
	}

	static Long getKey(int address, int keySize) {
		//In theory, cases 3,5,6,7 could overflow if the number lies right at the end of the buffer.
		//In practice, this is rare enough to not be an issue.
		return switch (keySize) {
			case 1 -> Globals.gameMemory.getByte(address) & 0xffL;
			case 2 -> Globals.gameMemory.getShort(address) & 0xffffL;
			case 3 -> (Globals.gameMemory.getInt(address) >>> Byte.SIZE) & 0xffffffL;
			case 4 -> Globals.gameMemory.getInt(address) & 0xffffffffL;
			case 5 -> Globals.gameMemory.getLong(address) >>> (Byte.SIZE * 3);
			case 6 -> Globals.gameMemory.getLong(address) >>> (Byte.SIZE * 2);
			case 7 -> Globals.gameMemory.getLong(address) >>> Byte.SIZE;
			case 8 -> Globals.gameMemory.getLong(address);
			default -> throw new IllegalArgumentException("Illegal keySize: " + keySize);
		};
	}

	@Override
	public boolean keyMatches(Long key) {
		return this.key == key;
	}

	@Override
	public boolean returnIndex() {
		return returnIndex;
	}

	@Override
	public boolean zeroKeyTerminates() {
		return zeroKeyTerminates;
	}
}
