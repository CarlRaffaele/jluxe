/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package jluxe.executor;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public class OpDumper {

	@SuppressFBWarnings(value = {"RCN_REDUNDANT_NULLCHECK_WOULD_HAVE_BEEN_A_NPE"}, justification = "False positive.")
	public static void main(String[] args) {
//		BlorbReader blorb;
//		UlxReader game;
//		String file = args[0];
//		File inFile = new File(file);
//		try {
//			switch (file.substring(file.lastIndexOf('.') + 1)) {
//				case "gblorb" -> {
//					blorb = BlorbReader.fromFile(inFile.toPath());
//					game = new UlxReader(blorb.getExec(0).orElseThrow(() ->
//							new IllegalArgumentException("Unable to find execution portion of blorb")));
//				}
//				case "ulx" -> {
//					FileChannel fileChannel = FileChannel.open(inFile.toPath());
//					game = new UlxReader(fileChannel.map(FileChannel.MapMode.READ_ONLY, 0,
//							fileChannel.size()));
//				}
//				default -> throw new IllegalArgumentException("Unknown file type in file: " + file);
//			}
//		} catch (IOException e) {
//			System.err.println("Unable to open file");
//			e.printStackTrace();
//			return;
//		}
//		Globals.memory = game.getGameData();
//		Globals.gluxHeader = game.getGluxHeader();
//		Globals.gluxDebuggingHeader = game.getGluxDebuggingHeader();
//		Globals.setupStack(Globals.gluxHeader.STACK_SIZE());
//		Parser.setDecodingTable(Globals.gluxHeader.DECODING_TABLE_ADDRESS());
//		Globals.pushCallFrameFromFunction(
//				Parser.parseFunction(Globals.gluxHeader.START_FUNCTION_ADDRESS()),
//				new int[0]
//		);
//
//		try (BufferedWriter os = Files.newBufferedWriter(Paths.get("opdump.txt"))) {
//			dumpInstructions(os);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//
//	@SuppressFBWarnings(value = {"REC_CATCH_EXCEPTION"},
//			justification = "Catching all exceptions is needed to continue dumping, even after an exception.")
//	private static void dumpInstructions(@NotNull Appendable out) throws IOException {
//		out.append(Globals.gluxHeader.toString()).append(System.lineSeparator());
//		out.append(Globals.gluxDebuggingHeader.toString()).append(System.lineSeparator());
//		Globals.IOSystemStream = cp -> {
//			try {
//				out.append(Character.toString(cp));
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		};
//		ByteBuffer mem = Globals.memory;
//
//		while (mem.position() < Globals.gluxHeader.RAM_START()) {
//			int address = mem.position();
//			if (address == Globals.gluxHeader.END_MEM()) {
//				out.append("-----ENDMEM-----");
//			}
//			out.append("0x").append(Integer.toHexString(address)).append(" ");
//			try {
//				switch (mem.get(address)) {
//					case (byte) 0xE0, (byte) 0xE1, (byte) 0xE2 -> out.append("<<")
//																	 .append(Parser.parseString(address))
//																	 .append(">>");
//					case (byte) 0xC0, (byte) 0xC1 -> out.append(Parser.parseFunction(mem).toString());
//					default -> {
//						try {
//							int opcode = Parser.getOPCode(mem);
//							out.append("\t");
//							switch (opcode) {
//								case 0x70, 0x71, 0x72, 0x73 -> {
//									try {
//										out.append('"');
//										Parser.parseOP(mem, opcode, address).run();
//										out.append('"');
//									} catch (Exception e) {
//										out.append("Error running stream: ").append(e.toString());
//									}
//								} //streamstr
//								default -> out.append(String.valueOf(Parser.parseOP(mem, opcode, address)));
//							}
//						} catch (IllegalArgumentException | NullPointerException | ArrayIndexOutOfBoundsException e) {
//							switch (mem.get(address)) {
//								case (byte) 0x60, 60 -> {
//									out.append("***Dictionary word***");
//									mem.get();
//								}
//								case (byte) 0x70, 70 -> {
//									out.append("***object or class***");
//									mem.get();
//								}
//								default -> out.append("!!!Unknown object: 0x")
//											  .append(Integer.toHexString(mem.get(address) & 0xff))
//											  .append("!!!");
//							}
//						}
//					}
//				}
//			} catch (Exception e) {
//				out.append(e.toString());
//			}
//			out.append(System.lineSeparator());
//		}
	}
}
