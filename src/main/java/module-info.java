module Jluxe {
	exports jluxe.executor;
	requires BlorbReader;
	requires static org.jetbrains.annotations;
	requires static com.github.spotbugs.annotations;
	requires org.apache.logging.log4j;
	requires trove4j;
	requires GameMemory;
	requires static Annotations;
}