Jluxe, a Java based interpreter for Inform games
=================

## Jluxe
Jluxe is a Java based implementation of the Glulx spec defined by Andrew Plotkin at [https://eblong.com/zarf/glulx/](https://eblong.com/zarf/glulx/). It accepts games written in Inform and compiled for Glulx.

## Supporting Modules
### GLK
Jluxe uses a Java implementation of the GLK librabary I've also created located [here](https://bitbucket.org/CarlRaffaele/simple-glk/). As of this writing the library only supports basic text; however, the full library implementation is planned.

### BlorbReader
To open and read .blorb files commonly used to package images and other media with game files the [BlorbReader](https://bitbucket.org/CarlRaffaele/blorbreader/) library is used.

## Building
Development is being done in Java 13; however, it should still compile with versions as far back as 9. Once Java 14 is released--along with the final version of JPackage--then the entire project will be built into one binary. Until then, both supporting modules above must be built along with this module. In addition, [IntelliJ annotations](https://github.com/JetBrains/java-annotations) are used throughout the project; however, they currently don't play well with modules, so a fork can be found [here](https://github.com/Glavo/java-annotations) which does.

## Running
jluxe takes one argument, the name of a .ulx or .blorb file to run. 